package testing;//import javafx.animation.KeyFrame;
//import javafx.animation.KeyValue;
//import javafx.animation.Timeline;
//import javafx.application.Application;
//import javafx.geometry.VPos;
//import javafx.scene.Scene;
//import javafx.scene.layout.Pane;
//import javafx.scene.text.Font;
//import javafx.scene.text.Text;
//import javafx.stage.Stage;
//import javafx.util.Duration;
//    //from  w  ww . j ava2  s  .  c om
//    public class testing.letthisboirun extends Application {
//        public static void main(String[] args) {
//            Application.launch(args);
//        }
//
//        @Override
//        public void start(Stage stage) {
//            Text msg = new Text("www.javafx\n oh yea");
//            msg.setTextOrigin(VPos.TOP);
//            msg.setFont(Font.font(24));
//
//            Pane root = new Pane(msg);
//            root.setPrefSize(500, 70);
//            Scene scene = new Scene(root);
//
//            stage.setScene(scene);
//            stage.setTitle("Scrolling Text");
//            stage.show();
////
//            double h = scene.getHeight();
//            double mh = msg.getLayoutBounds().getHeight();
////
//            KeyValue initKeyValue = new KeyValue(msg.translateYProperty(), -mh);
//            KeyFrame initFrame = new KeyFrame(Duration.ZERO, initKeyValue);
////
//            KeyValue endKeyValue = new KeyValue(msg.translateYProperty(), 2*h);
//            KeyFrame endFrame = new KeyFrame(Duration.seconds(2), endKeyValue);
////
//            Timeline timeline = new Timeline(initFrame, endFrame);
////
//            timeline.setCycleCount(Timeline.INDEFINITE);
//            timeline.play();
//        }
//    }
//import javafx.application.Application;
//import javafx.collections.*;
//import javafx.geometry.*;
//import javafx.scene.Scene;
//import javafx.scene.control.*;
//import javafx.scene.image.*;
//import javafx.scene.input.*;
//import javafx.scene.layout.VBox;
//import javafx.stage.Stage;
//
//import java.util.*;
//
//public class testing.letthisboirun extends Application {
//    private static final String PREFIX =
//            "http://icons.iconarchive.com/icons/jozef89/origami-birds/72/bird";
//
//    private static final String SUFFIX =
//            "-icon.png";
//
//    private static final ObservableList<String> birds = FXCollections.observableArrayList(
//            "-black",
//            "-blue",
//            "-red",
//            "-red-2",
//            "-yellow",
//            "s-green",
//            "s-green-2"
//    );
//
//    private static final ObservableList<Image> birdImages = FXCollections.observableArrayList();
//
//    @Override
//    public void start(Stage stage) throws Exception {
//        birds.forEach(bird -> birdImages.add(new Image(PREFIX + bird + SUFFIX)));
//
//        ListView<String> birdList = new ListView<>(birds);
//        birdList.setCellFactory(param -> new BirdCell());
//        birdList.setPrefWidth(180);
//
//        VBox layout = new VBox(birdList);
//        layout.setPadding(new Insets(10));
//
//        stage.setScene(new Scene(layout));
//        stage.show();
//    }
//
//    public static void main(String[] args) {
//        launch(testing.letthisboirun.class);
//    }
//
//    private class BirdCell extends ListCell<String> {
//        private final ImageView imageView = new ImageView();
//
//        public BirdCell() {
//            ListCell thisCell = this;
//
//            setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//            setAlignment(Pos.CENTER);
//
//            setOnDragDetected(event -> {
//                if (getItem() == null) {
//                    return;
//                }
//
//                ObservableList<String> items = getListView().getItems();
//
//                Dragboard dragboard = startDragAndDrop(TransferMode.MOVE);
//                ClipboardContent content = new ClipboardContent();
//                content.putString(getItem());
//                dragboard.setDragView(birdImages.get(items.indexOf(getItem())));
//                dragboard.setContent(content);
//
//                event.consume();
//            });
//
//            setOnDragOver(event -> {
//                if (event.getGestureSource() != thisCell && event.getDragboard().hasString()) {
//                    event.acceptTransferModes(TransferMode.MOVE);
//                }
//
//                event.consume();
//            });
//
//            setOnDragEntered(event -> {
//                if (event.getGestureSource() != thisCell &&
//                        event.getDragboard().hasString()) {
//                    setOpacity(0.3);
//                }
//            });
//
//            setOnDragExited(event -> {
//                if (event.getGestureSource() != thisCell && event.getDragboard().hasString()) {
//                    setOpacity(1);
//                }
//            });
//
//            setOnDragDropped(event -> {
//                if (getItem() == null) {
//                    return;
//                }
//
//                Dragboard db = event.getDragboard();
//                boolean success = false;
//
//                if (db.hasString()) {
//                    ObservableList<String> items = getListView().getItems();
//                    int draggedIdx = items.indexOf(db.getString());
//                    int thisIdx = items.indexOf(getItem());
//
//                    Image temp = birdImages.get(draggedIdx);
//                    birdImages.set(draggedIdx, birdImages.get(thisIdx));
//                    birdImages.set(thisIdx, temp);
//
//                    items.set(draggedIdx, getItem());
//                    items.set(thisIdx, db.getString());
//
//                    List<String> itemscopy = new ArrayList<>(getListView().getItems());
//                    getListView().getItems().setAll(itemscopy);
//
//                    success = true;
//                }
//                event.setDropCompleted(success);
//
//                event.consume();
//            });
//
//            setOnDragDone(DragEvent::consume);
//        }
//
//        @Override
//        protected void updateItem(String item, boolean empty) {
//            super.updateItem(item, empty);
//
//            if (empty || item == null) {
//                setGraphic(null);
//            } else {
//                imageView.setImage(
//                        birdImages.get(
//                                getListView().getItems().indexOf(item)
//                        )
//                );
//                setGraphic(imageView);
//            }
//        }
//    }

    // Iconset Homepage: http://jozef89.deviantart.com/art/Origami-Birds-400642253
    // License: CC Attribution-Noncommercial-No Derivate 3.0
    // Commercial usage: Not allowed

//}
import javafx.application.Application;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class letthisboirun extends Application {

    private Pane root = new Pane();
    private DoubleProperty mouseX = new SimpleDoubleProperty();
    private DoubleProperty mouseY = new SimpleDoubleProperty();

    private List<Circle> nodes;
    private Line currentLine = null;
    private boolean dragActive = false;

    private Parent createContent() {
        root.setPrefSize(600, 600);

        nodes = Arrays.asList(
                new Circle(50, 50, 50, Color.BLUE),
                new Circle(500, 100, 50, Color.RED),
                new Circle(400, 500, 50, Color.GREEN)
        );

        nodes.forEach(root.getChildren()::add);

        return root;
    }

    private Optional<Circle> findNode(double x, double y) {
        return nodes.stream().filter(n -> n.contains(x, y)).findAny();
    }

    private void startDrag(Circle node) {
        if (dragActive)
            return;

        dragActive = true;
        currentLine = new Line();
        currentLine.setUserData(node);
        currentLine.setStartX(node.getCenterX());
        currentLine.setStartY(node.getCenterY());
        currentLine.endXProperty().bind(mouseX);
        currentLine.endYProperty().bind(mouseY);

        root.getChildren().add(currentLine);
    }

    private void stopDrag(Circle node) {
        dragActive = false;

        if (currentLine.getUserData() != node) {
            // distinct node
            currentLine.endXProperty().unbind();
            currentLine.endYProperty().unbind();
            currentLine.setEndX(node.getCenterX());
            currentLine.setEndY(node.getCenterY());
            currentLine = null;
        } else {
            // same node
            stopDrag();
        }
    }

    private void stopDrag() {
        dragActive = false;

        currentLine.endXProperty().unbind();
        currentLine.endYProperty().unbind();
        root.getChildren().remove(currentLine);
        currentLine = null;
    }

    private void attachHandlers(Scene scene) {
        scene.setOnMouseMoved(e -> {
            mouseX.set(e.getSceneX());
            mouseY.set(e.getSceneY());
        });

        scene.setOnMouseDragged(e -> {
            mouseX.set(e.getSceneX());
            mouseY.set(e.getSceneY());
        });

        scene.setOnMousePressed(e -> {
            findNode(e.getSceneX(), e.getSceneY()).ifPresent(this::startDrag);
        });

        scene.setOnMouseReleased(e -> {
            Optional<Circle> maybeNode = findNode(e.getSceneX(), e.getSceneY());
            if (maybeNode.isPresent()) {
                stopDrag(maybeNode.get());
            } else {
                stopDrag();
            }
        });
    }

    @Override
    public void start(Stage stage) throws Exception {
        Scene scene = new Scene(createContent());

        attachHandlers(scene);

        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}