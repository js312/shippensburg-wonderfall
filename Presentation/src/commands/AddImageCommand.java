package commands;

import controllers.WonderFallController;
import javafx.scene.control.ListView;
import model.ElementList;
import model.StreamElement;


public class AddImageCommand implements CommandInterface{
    ListView timeline;
    StreamElement image;

    /**
     * @param timeline listview we wish to add to
     * @param image the selected image we wish to add
     */
    public AddImageCommand(ListView timeline, StreamElement image){
        this.image = image;
        this.timeline = timeline;
    }

    /**
     * Adds the selected image to the listview
     */
    public void execute() {
        ElementList images = ElementList.getInstance();
        images.addElement(image);
        timeline.setItems(images.getList());
        WonderFallController.dec.execute();
    }
}
