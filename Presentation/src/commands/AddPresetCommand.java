package commands;

import controllers.WonderFallController;
import javafx.scene.image.Image;
import model.ElementList;
import javafx.scene.control.ListView;
import model.StreamImage;


public class AddPresetCommand implements CommandInterface{
    private static final String PATH = "Preset_Images/";
    private static final String TYPE_PNG = ".png";
    private final ListView timeline;
    private final String choice;

    public AddPresetCommand(String choice, ListView timeline){
        this.choice = choice;
        this.timeline = timeline;
    }

    /**
     * Adds a preset image to the timeline
     */
    @Override
    public void execute(){
        Image image = new Image( PATH+choice+TYPE_PNG);
        StreamImage convertedImage = new StreamImage(image, choice+TYPE_PNG);
        ElementList images = ElementList.getInstance();
        images.addElement(convertedImage);
        timeline.setItems(images.getList());
        WonderFallController.dec.execute();
    }
}
