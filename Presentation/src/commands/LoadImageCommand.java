package commands;

import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.StreamElement;
import model.StreamImage;
import popup.ErrorPopup;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class LoadImageCommand implements CommandInterface {

    private final ListView timeline;

    /**
     * @param timeline the timeline to be added to
     */
    public LoadImageCommand(ListView timeline) {
        this.timeline = timeline;
    }

    /**
     * Gets an image from the PC and adds it to the timeline
     * if its a wrong file type, we will have an error popup
     */
    @Override
    public void execute() {
        try {
            File file;
            FileChooser fileChooser = new FileChooser();
            file = fileChooser.showOpenDialog(new Stage());
            if (file != null) {
                String extension = "";
                int i = file.getPath().lastIndexOf('.');
                if (i >= 0) {
                    extension = file.getPath().substring(i + 1);
                }
                if (extension.equalsIgnoreCase("jpeg") || extension.equalsIgnoreCase("png")
                        || extension.equalsIgnoreCase("jpg")) { //bmp,gif is also supported by image
                    System.out.println(file.getPath());
                    Image image = new Image(new FileInputStream(file.getPath()));
                    StreamElement convertedImage = new StreamImage(image, file.getName());
                    new AddImageCommand(timeline, convertedImage).execute();
                    return;
                }
            }
            throw new FileNotFoundException();
        } catch (FileNotFoundException e) {
            new ErrorPopup("Wrong File Type Error", "Unsupported file type selected\nSupported file types: .png .jpg");
        }
    }

}

