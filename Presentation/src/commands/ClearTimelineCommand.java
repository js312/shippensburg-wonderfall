package commands;

import controllers.WonderFallController;
import javafx.scene.text.Text;
import model.ElementList;

import java.awt.*;

/**
 * Command to delete everything from the timeline
 */
public class ClearTimelineCommand implements CommandInterface{
    private Text elementPreview;

    /**
     * Constructor to link the element preview
     * @param elementPreview the elementPreview
     */
    public ClearTimelineCommand(Text elementPreview){
        this.elementPreview = elementPreview;
    }

    /**
     * removes all elements from the list
     */
    @Override
    public void execute() {
        ElementList images = ElementList.getInstance();
        while(!images.getList().isEmpty()){
            images.getList().remove(0);
        }
        elementPreview.setText(null);
        WonderFallController.dec.execute();
    }
}
