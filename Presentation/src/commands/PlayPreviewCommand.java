package commands;

import popup.PreviewDisplay;

public class PlayPreviewCommand implements CommandInterface{

    public PlayPreviewCommand() {}

    /**
     * When the play button is pressed, all elements in the listview is played
     */
    public void execute(){
        new PreviewDisplay();
    }

}
