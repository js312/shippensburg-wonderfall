package commands;

import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import model.ElementList;
import model.StreamElement;

public class SetListCommand implements CommandInterface {
    ObservableList<StreamElement> elements;
    ListView timeline;

    /**
     * Constructor for SetList
     * @param elements The elements to add
     * @param timeline The timeline to update
     */
    public SetListCommand(ObservableList<StreamElement> elements, ListView timeline) {
        this.elements = elements;
        this.timeline = timeline;
    }

    /**
     * Updates the timeline with the story loaded
     */
    @Override
    public void execute() {
        ElementList.getInstance().setList(elements);
        timeline.setItems(ElementList.getInstance().getList());
        double max = 0;
        for (StreamElement e: ElementList.getInstance().getList()) {
            if (max < e.getID()) {
                max = e.getID();
            }
        }
        ElementList.getInstance().getList().get(0).setNextId(max+1);
    }
}
