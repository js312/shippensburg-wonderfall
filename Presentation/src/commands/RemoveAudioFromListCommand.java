package commands;

import javafx.scene.control.ListView;
import model.StreamElement;

/**
 * A command to remove the selected audio from the list that holds it
 */
public class RemoveAudioFromListCommand implements CommandInterface {
    private ListView audioList;
    public RemoveAudioFromListCommand(ListView audioList) {
        this.audioList = audioList;
    }

    @Override
    public void execute() {
        final int selectedId = audioList.getSelectionModel().getSelectedIndex();
        if (selectedId != -1) {
            audioList.getItems().remove(selectedId);
            StreamElement.getAudioPaths().remove(selectedId);
        }
    }
}
