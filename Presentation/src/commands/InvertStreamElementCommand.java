package commands;

import model.StreamElement;

public class InvertStreamElementCommand implements CommandInterface {
    StreamElement streamElement;

    /**
     * @param se The current StreamElement to invert
     */
    public InvertStreamElementCommand(StreamElement se){
        streamElement = se;
    }

    /**
     * Inverts the Element
     */
    public void execute(){
        streamElement.invertStreamElement();
    }
}
