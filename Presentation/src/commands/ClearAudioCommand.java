package commands;

import controllers.WonderFallController;
import javafx.scene.control.ListView;
import model.StreamElement;

/**
 * Command to remove audio from every element
 */
public class ClearAudioCommand implements CommandInterface{
    private ListView timeline;

    /**
     * Constructor to link the timeline to the command
     * @param timeline the timeline
     */
    public ClearAudioCommand(ListView timeline) {
        this.timeline = timeline;
    }

    @Override
    public void execute() {
        for (int i = 0; i < timeline.getItems().size(); i++) {
            StreamElement element = (StreamElement) timeline.getItems().get(i);
            element.setAudioTrack("");
        }
        WonderFallController.dec.execute();
    }
}
