package commands;

import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.SaveLoadSystem;
import model.StreamElement;

import java.io.File;
import java.util.NoSuchElementException;

/**
 * A command that loads a previously stored state of the timeline
 */
public class LoadStoryCommand implements CommandInterface {
    ListView timeline;

    /**
     * constructor for load story by passing in the timeline singleton
     * @param timeline the timeline to add
     */
    public LoadStoryCommand(ListView timeline) {
        this.timeline = timeline;
    }

    /**
     * Loads all of the stored stream objects in a given file into an observational list
     * @return the observational list of ImageClasses
     */
    @Override
    public void execute() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Load Story");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Serialized Story Files", "*.ser"));
        File storyFile = fileChooser.showOpenDialog(new Stage());
        if (storyFile != null) {
            ObservableList<StreamElement> newList = SaveLoadSystem.loadStory(storyFile);
            try {
                new SetListCommand(newList, timeline).execute();
            } catch (NoSuchElementException e) {
                System.out.println("User closed load window");
            }
        }
    }
}
