package commands;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.StreamElement;

/**
 * A command to make the ColorPicker for an element
 * @author Aaron, Eric, Jake, June, Logan
 */
public class ColorPickerCommand implements CommandInterface {
    private StreamElement element;
    private ListView timeline;

    /**
     * Creates a new ColorPickerCommand
     * @param timeline the timeline
     * @param element the selected element
     */
    public ColorPickerCommand(ListView timeline, StreamElement element) {
        this.timeline = timeline;
        this.element = element;
    }

    @Override
    public void execute() {
        ColorPicker cp = new ColorPicker();
        cp.setLayoutY(18);
        Label pick = new Label("Pick new color:");
        Pane root = new Pane(cp);
        root.getChildren().add(pick);
        pick.setAlignment(Pos.CENTER);
        root.setPrefSize(180, 50);
        cp.layoutXProperty().bind(root.widthProperty().divide(2).subtract(cp.widthProperty().divide(2)));
        cp.setValue(element.getColor());
        pick.layoutXProperty().bind(root.widthProperty().subtract(pick.widthProperty()).divide(2));
        Scene scene = new Scene(root);

        Stage stage = new Stage();
        stage.setTitle("Set color");
        stage.setScene(scene);
        Image i = new Image("resources/Shippensburg_State_University_Logo.png");
        stage.getIcons().add(i);
        stage.show();
        stage.setResizable(false);

        cp.setOnAction(t -> {
            element.setColor(cp.getValue());
            timeline.refresh();
        });

    }
}
