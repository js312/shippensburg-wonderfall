package commands;

import javafx.scene.control.ListView;
import javafx.scene.text.Text;

public class RemoveElementCommand {
    ListView timeline;
    int selectedIdx;
    Text elementPreview;
    /**
     * Constructor for RemoveElement
     * @param timeline listview that contains all elements
     * @param selectedIdx index of the selected element
     * @param elementPreview image in text form
     */
    public RemoveElementCommand(ListView timeline,int selectedIdx, Text elementPreview){
        this.timeline = timeline;
        this.selectedIdx = selectedIdx;
        this.elementPreview = elementPreview;
    }

    /**
     * Removes the item from the list when delete is pressed.
     */
    public void execute(){
        final int newSelectedIdx = (selectedIdx == timeline.getItems().size() - 1) ? selectedIdx - 1 : selectedIdx;
        if (selectedIdx != -1) {
            timeline.getItems().remove(selectedIdx);
            timeline.getSelectionModel().select(newSelectedIdx);
            elementPreview.setText(null);
        }

    }
}
