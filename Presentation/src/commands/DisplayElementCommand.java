package commands;

import controllers.WonderFallController;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import model.ElementList;
import model.StreamElement;

import java.awt.*;
import javafx.scene.control.Button;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class DisplayElementCommand implements CommandInterface{
    private ListView timeline;
    private Text elementPreview;
    private TextField currAudioName;
    private ColorPicker colorPicker;
    private ListView audioList;
    private TextField spanNumber;
    private Button removeAllAudio;
    private Button removeAllElements;

    /**
     *
     * @param timeline listview that contains all elements
     * @param elementPreview image in text form
     */
    public DisplayElementCommand(ListView timeline, Text elementPreview, TextField currAudioName,
                                 ColorPicker colorPicker, ListView audioList, TextField spanNumber,
                                 Button removeAllAudio, Button removeAllElements) {
        this.timeline = timeline;
        this.elementPreview = elementPreview;
        this.currAudioName = currAudioName;
        this.colorPicker = colorPicker;
        this.audioList = audioList;
        this.spanNumber = spanNumber;
        this.removeAllAudio = removeAllAudio;
        this.removeAllElements = removeAllElements;
    }

    /**
     * When a item in the listview is clicked, it displays the image of the item in preview.
     */
    public void execute(){
        ElementList images = ElementList.getInstance();
        if (!images.getList().isEmpty()) {
            int index = timeline.getSelectionModel().getSelectedIndex();
            if (index != -1) {
                StreamElement i = (StreamElement) timeline.getItems().get(index);
                if(!WonderFallController.getInUser()) {
                    audioList.getItems().clear();
                    Path p = Paths.get(i.getAudioTrack());
                    String name = p.getFileName().toString();
                    currAudioName.setText(name);
                    for (String audio : StreamElement.getAudioPaths()) {
                        p = Paths.get(audio);
                        name = p.getFileName().toString();
                        audioList.getItems().add(name);
                        if (audio == i.getAudioTrack()) {
                            audioList.getSelectionModel().selectLast();
                        }
                    }
                    if (currAudioName.getText().equals("")) {
                        audioList.getSelectionModel().selectFirst();
                    }
                    spanNumber.setText("1");
                }

                colorPicker.getParent().setVisible(true);
                colorPicker.setValue(i.getColor());
                colorPicker.setOnAction(t -> {
                    i.setColor(colorPicker.getValue());
                    timeline.refresh();
                });
                elementPreview.setText(i.getTextImage());
                elementPreview.setLineSpacing(-0.04);
            }
            removeAllElements.setVisible(true);
            if(!WonderFallController.getInUser()) {
                removeAllAudio.setVisible(true);
            }
        } else {
            removeAllElements.setVisible(false);
            if(!WonderFallController.getInUser()) {
                removeAllAudio.setVisible(false);
                audioList.getParent().setVisible(false);
            } else {
                colorPicker.getParent().setVisible(false);
            }
        }
        timeline.refresh();
    }
}
