package commands;

import javafx.collections.ObservableList;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.SaveLoadSystem;
import model.StreamElement;
import java.io.File;

/**
 * A command for saving the current state of the timeline
 */
public class SaveStoryCommand implements CommandInterface {
    private ObservableList<StreamElement> list;

    /**
     * A constructor that takes in the list of stream elements
     * @param list the list of stream elements
     */
    public SaveStoryCommand(ObservableList<StreamElement> list) {
        this.list = list;
    }

    /**
     * Calls the save story method with the passed in instance variables
     * @return returns an empty optional, not needed
     */
    @Override
    public void execute() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Story");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Serialized Story Files", "*.ser"));
        File file = fileChooser.showSaveDialog(new Stage());
        if (file != null) {
            SaveLoadSystem.saveStory(list, file);
        }

    }
}
