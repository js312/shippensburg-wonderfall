package commands;

import javafx.scene.control.ListView;
import model.StreamElement;
import popup.ErrorPopup;

public class ApplyAudioCommand implements CommandInterface {
    private ListView timeline;
    private ListView audioList;
    private final int spanNum;

    public ApplyAudioCommand(ListView timeline, ListView audioList, int spanNum) {
        this.timeline = timeline;
        this.audioList = audioList;
        this.spanNum = spanNum;
    }

    /**
     * Applies audio to an element
     */
    @Override
    public void execute() {
        final int selectedId = audioList.getSelectionModel().getSelectedIndex();
        final int selectedIdTimeline = timeline.getSelectionModel().getSelectedIndex();
        int elementSpan = selectedIdTimeline + spanNum;
        if ( selectedId != -1) {
            if (elementSpan > timeline.getItems().size()) {
                elementSpan = timeline.getItems().size();
            }
            for (int i = selectedIdTimeline; i < elementSpan; i++) {
                StreamElement element = (StreamElement) timeline.getItems().get(i);
                element.setAudioTrack(StreamElement.getAudioPaths().get(selectedId));
            }
        } else {
            new ErrorPopup("Error", "Please load an audio file from the left bar");
        }
        timeline.refresh();
    }
}
