package Runnables;

import SSH.SSHTunnel;
import controllers.WonderFallController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;




public class
Main extends Application{
    private static String fxml = "wonderfall";
    /**
     * Loads the FXML file and shows the main application.
     *
     * @param primaryStage
     * @throws Exception
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("resources/" + fxml + ".fxml"));
        primaryStage.setTitle("WonderFall");

        Image i = new Image("resources/Shippensburg_State_University_Logo.png");
        primaryStage.getIcons().add(i);
        primaryStage.setScene(new Scene(root));

        if(fxml.equals("wonderfallUser")) {
            WonderFallController.setInUser(true);
        }

       // SSHTunnel sshConnect = new SSHTunnel();
       // sshConnect.createConnection();
       // primaryStage.setOnCloseRequest(e -> {
        //    sshConnect.closeConnection();
       // });
        primaryStage.show();
    }

    public static void setFxml(String mode) {
        fxml = mode;
    }
    /**
     * Starts the program.
     * @param args
     */
    public static void main(String[] args) {
        try {
            fxml = args[0];
        } catch (IndexOutOfBoundsException e) {
            String[] tempArgs = { "wonderfall"};
            args = tempArgs;
        }
        launch(args);
    }
}
