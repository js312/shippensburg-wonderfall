package Runnables;

import javafx.application.Platform;
import popup.PasswordPopup;

import java.io.IOException;

public class JarMain {
    public static void main(String[] args) {
        Platform.startup(() -> {
        Platform.runLater(() -> {
            try {
                new PasswordPopup();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        });
    }
}
