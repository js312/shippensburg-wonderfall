package controllers;

import commands.AddImageCommand;
import commands.RemoveElementCommand;
import exceptions.StringTooLongException;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import model.StreamElement;
import model.StreamText;
import popup.ErrorPopup;
import string.FancyString;
import java.awt.*;



public class TextPopupController {
    @FXML private ListView<StreamText> stringsList;
    private ListView<StreamElement> lv;
    @FXML private TextField tf;
    @FXML private Button cancelButton;
    @FXML private Button confirmButton;
    @FXML private ComboBox<String> fontComboBox;
    @FXML private ComboBox<Integer> sizeComboBox;


    private String fonts[] = {"Monospaced", "Dialog", "Serif"};
    private int sizes[] = {14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36};



    /**
     * Adds text to the ListView
     */
    @FXML
    private void addToList() {
        if(tf.getText().length() < 1){
            return;
        }
        try {
            stringsList.getItems().add(new StreamText(new FancyString(tf.getText(), new Font(fontComboBox.getValue(), Font.PLAIN, sizeComboBox.getValue()))));
            tf.setText("");
        } catch (StringTooLongException e) {
            new ErrorPopup("String too long", "The message you want to print was too long for the current font size.");
        }

    }

    /**
     * Checks to see if control key is down.
     * @param kp the current keys pressed down
     */
    @FXML
    private void checkCTRL(KeyEvent kp){
        if(kp.isControlDown()){
            tf.setEditable(false);
        }
    }

    /**
     * Will not release lock if control is still pressed.
     * @param kp the current keys pressed down
     */
    @FXML
    private void releaseEditableLock(KeyEvent kp){
        if(kp.isControlDown()) {
            return;
        }
        tf.setEditable(true);
    }

    /**
     * Adds the items to the main stream
     */
    @FXML
    private void addToStream() {
        for(StreamText s: stringsList.getItems()){
            new AddImageCommand(lv, s).execute();
        }

        Stage stage = (Stage) confirmButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Removes the all of the Strings from the view and Closes the Stage.
     */
    @FXML
    void cancel() {
        stringsList.getItems().removeAll(stringsList.getItems());
        Stage stage = (Stage) cancelButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Deletes the string from the view
     */
    @FXML
    void deleteText(){
        new RemoveElementCommand(stringsList, stringsList.getSelectionModel().getSelectedIndex(), new Text()).execute();
    }

    /**
     * Runs when the window is launched
     */
    public void initialize(){
        for(String x : fonts){
            fontComboBox.getItems().add(x);
        }
        fontComboBox.setValue(fonts[0]);
        for(int x : sizes){
            sizeComboBox.getItems().add(x);
        }
        sizeComboBox.setValue(sizes[0]);
    }

    /**
     * Links the Main time line to this window.
     * @param timeline  The main time line
     */
    public void linkTimeline(ListView timeline) { lv = timeline; }

}
