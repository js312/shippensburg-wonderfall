package controllers;

import commands.ApplyAudioCommand;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.StreamElement;
import popup.ErrorPopup;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;

/**
 * The controller for the Audio Popup
 */
public class AudioController implements Initializable {
    @FXML ListView audioList;
    @FXML TextField spanNumber;
    private ListView timeline;

    /**
     * Adds a Audio clip to the elements in the timeline
     */
    public void applyAudio() {
       try {
           new ApplyAudioCommand(timeline, audioList, Integer.parseInt(spanNumber.getText())).execute();
           Stage stage = (Stage) audioList.getScene().getWindow();
           stage.close();
       } catch (NumberFormatException e) {
           new ErrorPopup("Error", "Please enter a valid number");
       }
    }

    /**
     * Links the Main time line to this window.
     * @param timeline  The main time line
     */
    public void linkTimeline(ListView timeline) { this.timeline = timeline; }


    /**
     * Initializes the Audio Popup and adds the audio clips for a user to select
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        for (String audio : StreamElement.getAudioPaths()) {
            Path p = Paths.get(audio);
            String name = p.getFileName().toString();
            audioList.getItems().add(name);
        }
        audioList.getSelectionModel().selectFirst();
    }
}
