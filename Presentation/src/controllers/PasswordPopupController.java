package controllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.stage.Stage;
import popup.ErrorPopup;
import Runnables.Main;


/**
 * The controller for the password popup
 */
public class PasswordPopupController {
    @FXML
    private PasswordField pf;

    /**
     * Checks if the password given is the correct password
     */
    public void checkIfCorrect() {
        //Try to check the password
        if (pf.getText().equals("Password")) {
            Platform.runLater(() -> {
                try {
                    Stage stage = (Stage) pf.getScene().getWindow();
                    stage.close();
                    new Main().start(new Stage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

        } else {
            pf.setText("");
            new ErrorPopup("Password mismatch", "Incorrect Password, Try Again");
        }
    }

    /**
     * Switches to user mode if usermode is clicked
     */
    public void switchUser() {
        try {
            Stage stage = (Stage) pf.getScene().getWindow();
            stage.close();
            Main.setFxml("wonderfallUser");
            new Main().start(new Stage());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
