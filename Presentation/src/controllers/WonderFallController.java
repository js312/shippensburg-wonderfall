package controllers;


import SSHCommands.SendAudioCommand;
import SSHCommands.WriteFileCommand;
import commands.*;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

import javafx.stage.FileChooser;
import javafx.stage.Stage;
import model.ElementList;
import model.NativeUtils;
import org.opencv.core.Core;
import popup.*;

import model.StreamElement;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

/**
 * Controller for the Main GUI
 */
public class WonderFallController {
    @FXML
    private ListView timeline = new ListView<StreamElement>();
    public Button yea;
    public Button nah;
    public Text textPreview;
    public Optional<StreamElement> image = null;
    @FXML private Text elementPreview;
    public Button previewButton;
    private boolean opencvIsLoaded = false;
    @FXML private ListView audioList;
    @FXML private TextField spanNumber;
    @FXML private TextField currAudioName;
    @FXML private ColorPicker colorPicker;
    @FXML public Button removeAllAudio;
    @FXML public Button removeAllElements;
    public static DisplayElementCommand dec;
    private static boolean inUser = false;

    public static void setInUser(boolean user) {
        inUser = user;
    }

    public static boolean getInUser() {
        return inUser;
    }

    /**
     * Initialize the GUI
     */
    @FXML
    public void initialize() {
        timeline.setCellFactory(x -> new StreamElementCell());
        dec = new DisplayElementCommand(timeline, elementPreview, currAudioName, colorPicker, audioList,
                spanNumber, removeAllAudio, removeAllElements);
    }

    /**
     * This will open file explorer and display the chosen image then ask for confirmation.
     *
     * @param actionEvent mouse on click event.
     */
    public void addImageOnClick(ActionEvent actionEvent){
        // iteration 1 keepsake
        System.out.println("the button was clicked");
        new LoadImageCommand(timeline).execute();
        updateGUI();
    }
    /**
     * Creates the File of the timeline and Sends the file through the SSH tunnel
     * @param event On mouse Clicked
     */
    @FXML
    void sendFIle(ActionEvent event) {
        new SendAudioCommand(timeline).execute();
        new WriteFileCommand(timeline).execute();
    }


    /**
     * Opens the help popup for power mode
     * @param event when the button is clicked
     */
    @FXML
    void helpPopup(ActionEvent event) {
        try{
            new HelpPopup();
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Opens the help menu for the User mode
     * @param event when the item is clicked
     */
    @FXML
    void helpPopupUser(ActionEvent event) {
        try{
            new UserHelpPopup();
        }catch(IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * If yes was clicked:
     * - preview of the image disappears.
     * - the buttons become hidden.
     * - the image is added to listview.
     *
     * @param actionEvent mouse on click event.
     */
    public void addToList(ActionEvent actionEvent) {
        AddImageCommand command = new AddImageCommand(timeline, image.get());
        command.execute();
        System.out.println("The prompt image is added to the list");
        textPreview.setText(null);
        toggleButtons();
        updateGUI();
    }

    /**
     * If no was clicked:
     * - preview of the image disappears.
     * - the buttons become hidden.
     *
     * @param actionEvent mouse on click event.
     */
    public void Reset(ActionEvent actionEvent) {
        textPreview.setText(null);
        System.out.println("The image has been declined");
        toggleButtons();
    }

    /**
     * Removes the item from the list when delete is pressed.
     *
     * @param actionEvent mouse on click event.
     */
    public void removeElement(ActionEvent actionEvent) {
        final int selectedIdx = timeline.getSelectionModel().getSelectedIndex();
        RemoveElementCommand removeElementCommand = new RemoveElementCommand(timeline, selectedIdx, elementPreview);
        removeElementCommand.execute();
        updateGUI();
    }

    /**
     * Hides the buttons.
     */
    private void toggleButtons() {
        yea.setVisible(false);
        nah.setVisible(false);
    }

    /**
     * used for later implementation of menu dropdown settings.
     *
     * @param actionEvent TBD.
     */
    public void hide(ActionEvent actionEvent) {
        toggleButtons();
    }

    /**
     * When a item in the listview is clicked, it displays the image of the item in preview.
     *
     * @param mouseEvent mouse on click event.
     */
    public void displayElement(MouseEvent mouseEvent) {

        updateGUI();
    }

    /**
     * When the play button is pressed, all elements in the listview is played
     *
     * @param actionEvent mouse on click event.
     */
    public void playPreview(ActionEvent actionEvent) {
        new PlayPreviewCommand().execute();
    }

    /**
     * Runs the save story command execute method and passes in the current list of stream elements
     *
     * @param actionEvent mouse on click event.
     */
    public void saveStory(ActionEvent actionEvent) {
        SaveStoryCommand saveCommand = new SaveStoryCommand(ElementList.getInstance().getList());
        saveCommand.execute();
    }

    /**
     * Opens a filechooser that only accepts serialized binary files, then uses the saveload system to save
     * the story into the list of images.
     *
     * @param actionEvent mouse on click event.
     */
    public void loadStory(ActionEvent actionEvent) {
        LoadStoryCommand loadStory = new LoadStoryCommand(timeline);
        loadStory.execute();
    }

    /**
     * Opens a window that has preset images ready to choose from.
     *
     * @param actionEvent when a button is clicked, the image should be added to the timeline.
     * @throws Exception
     */
    public void openPresetWindow(ActionEvent actionEvent) throws Exception {
        try {
            PresetPopup presetPopup = new PresetPopup(timeline);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void openTextWindow() {
        try {
            TextPopup presetPopup = new TextPopup(timeline);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   public void clearTimeline() {
       try {
           CommandInterface command = new ClearTimelineCommand(elementPreview);
           ConfirmationPopup confirmationPopup = new ConfirmationPopup(command);
           updateGUI();
       } catch (Exception e) {
           e.printStackTrace();
       }
    }
    /**
     * Opens a window to use face camera and take picture of face to use in wonderfall
     */
    public void addVideo() {
        if (!opencvIsLoaded) {
            try {
                System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
            } catch (UnsatisfiedLinkError e) {
                try {
                    NativeUtils.loadLibraryFromJar("/" + System.mapLibraryName(Core.NATIVE_LIBRARY_NAME));
                } catch (IOException e1) {
                    throw new RuntimeException(e1);
                }
            }

            opencvIsLoaded = true;
        }
        try {
            VideoPopup videoPopup = new VideoPopup(timeline);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Opens a file explorer for the the user to navigate to and choose the audio they want
     * Checks to make sure its a .wav or .mp3
     */
    public void chooseAudio() {
        File file = null;
        FileChooser fileChooser = new FileChooser();
        file = fileChooser.showOpenDialog(new Stage());
        if (file != null) {
            String extension = "";
            int i = file.getPath().toString().lastIndexOf('.');
            if (i >= 0) {
                extension = file.getPath().toString().substring(i + 1);
            }
            if (extension.equals("wav") || extension.equals("mp3")) {
                System.out.println(file.getPath());
                StreamElement.getAudioPaths().add(file.getPath());
                updateGUI();
            }
        }
    }

    /**
     * Inverts the element selected.
     */
    public void invertElement() {
        final int selectedIdx = timeline.getSelectionModel().getSelectedIndex();
        if (selectedIdx == -1) {
            return;
        }
        new InvertStreamElementCommand((StreamElement) timeline.getItems().get(selectedIdx)).execute();
        updateGUI();
    }

    /**
     * Resets the color at the selected index.
     */
    public void removeColor() {
        final int selectedIdx = timeline.getSelectionModel().getSelectedIndex();
        if (selectedIdx == -1) {
            return;
        }
        StreamElement element = (StreamElement) timeline.getItems().get(selectedIdx);
        element.setColor(Color.TRANSPARENT);
        updateGUI();
    }

    /**
     * Removes an Audio from an element
     * @param actionEvent
     */
    public void removeAudio(ActionEvent actionEvent) {
        final int selectedIdx = timeline.getSelectionModel().getSelectedIndex();
        if (selectedIdx == -1) {
            return;
        }
        StreamElement element = (StreamElement) timeline.getItems().get(selectedIdx);
        element.setAudioTrack("");
        updateGUI();
    }

    /**
     * removes an audio track from the list of usable audios
     */
    public void removeAudioFromList() {
        RemoveAudioFromListCommand command = new RemoveAudioFromListCommand(audioList);
        command.execute();
        updateGUI();
    }

    /**
     * Removes audio from all elements in the timeline
     * @param actionEvent
     */
    public void removeAllAudio(ActionEvent actionEvent) {
        try {
            CommandInterface command = new ClearAudioCommand(timeline);
            ConfirmationPopup confirmationPopup = new ConfirmationPopup(command);
            updateGUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Applies the selected audio to the selected element
     */
    public void applyAudio() {
        try {
            new ApplyAudioCommand(timeline, audioList, Integer.parseInt(spanNumber.getText())).execute();
            updateGUI();
        } catch (NumberFormatException e) {
            new ErrorPopup("Error", "Please enter a valid number");
        }
    }

    /**
     * Refreshes the GUI so that settings are shown or taken away when needed
     */
    public void updateGUI() {
        DisplayElementCommand displayElementCommand = new DisplayElementCommand(timeline, elementPreview, currAudioName, colorPicker, audioList,
                spanNumber, removeAllAudio, removeAllElements);
        displayElementCommand.execute();
    }

    /**
     * Creates the File of the timeline and Sends the file through the SSH tunnel
     * @param event On mouse Clicked
     */
    @FXML
    void sendFile(ActionEvent event) {
        new SendAudioCommand(timeline).execute();
        new WriteFileCommand(timeline).execute();
    }

}
