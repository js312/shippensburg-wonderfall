package controllers;

import javafx.animation.AnimationTimer;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.ElementList;

import java.io.File;
import java.util.Arrays;
import java.util.Iterator;

import model.StreamElement;
import pixel.Pixel;
import pixel.PixelRow;
import javafx.event.Event;
import javafx.event.ActionEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javafx.event.EventHandler;
import compression.*;

public class PreviewController {
    final int HEIGHT = 750;
    final double minTime = 1.0/45;  //denominator is the drops/sec
    private ElementList elementList;
    Stage stage;
    List<AnimationTimer> timerList = new ArrayList<AnimationTimer>();
    int current = 0;
    int timerNum = 0;
    Media media;
    MediaPlayer player;
    String prevAudio = "";
    boolean stopped = false;
    final private double width = 5;           //pixels wide.  width of the window/number of valves
    final private double dropWidth = .2;      //the diameter of the valve
    final private double sectionWidth = .625; //Number of inches wide / number of valves

    @FXML
    private void draw() {
        AnchorPane ap = new AnchorPane();
        ap.setPrefWidth(StreamElement.NUM_OF_PIXELS * 5);
        ap.setPrefHeight(HEIGHT);
        stage.setScene(new Scene(ap));

        elementList.getList().forEach(streamElement -> {
            timerList.add(setAnimationTimer(streamElement, ap));
            timerNum++;
        });

        String path = elementList.getList().get(0).getAudioTrack();
        if (!path.equals("")) {
            media = new Media(new File(path).toURI().toString());
            player = new MediaPlayer(media);
            player.setAutoPlay(true);
            player.setVolume(.3);
        }

        stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                if (player != null) {
                    player.stop();
                }
                stopped = true;
            }
        });

        Event.fireEvent(stage, new Event(ActionEvent.ACTION));
    }

    private EventHandler<ActionEvent> newAnimation(){
        return new EventHandler(){
            @Override
            public void handle(Event event) {
                if (stopped) {
                    current = timerNum;
                }
                if (current < timerNum) {
                    if (current != 0) {
                        checkPreviousAudio(elementList.getList().get(current));
                    }
                    timerList.get(current).start();
                    current++;
                } else {
                    if (player != null) {
                        player.stop();
                    }
                }
            }
        };
    }

    private void startTimer(AnimationTimer timer) {
        timer.start();
    }

    /**
     * Creating an animation timer
     * @param se the stream element
     * @param pane the pane it will be added to
     * @return the animation timer so it can be ran later.
     */
    private AnimationTimer setAnimationTimer(StreamElement se, Pane pane) {
        return new AnimationTimer() {
            List<PixelRow> pixelRowList = new ArrayList<PixelRow>();
            boolean[] valves = off();
            Pixel[] pixels = getNewPixelArray(se.getColor(), pane);
            int counter = 0;
            int height = se.getImageArray().length;
            long lastDropReleased = System.currentTimeMillis();
            long lastIteration = System.currentTimeMillis();
            double time = 0.022;
            //double time = new FullCompression(se).calculateFullDropRate();
            boolean first = true;
            private static final double GRAVITY = 3200;
            private static final double HALFGRAVITY = GRAVITY / 2;

            double[] compressionTimes = new VariableCompression(se).createTimeArray();

            public void handle(long l) {

                long currentTime = System.currentTimeMillis();
                double elapsedTimeMS = currentTime - lastIteration;
                double elapsedTimeS = elapsedTimeMS / 1000;
                pixelRowList.forEach(x -> x.calculateNewY(elapsedTimeS));
                //System.out.println(elapsedTimeS);


                if(counter <= height && (((currentTime - lastDropReleased) >= (time * 1000)) || first)) {
                    if(first){
                        pixelRowList.add(new PixelRow());
                        first = false;
                    }
                    else {
                        if(counter != height) {
                            time = compressionTimes[height - counter - 1];
                            //first = false;
                            valves = se.getImageArray()[se.getImageArray().length - counter - 1];
                        }
                        PixelRow pixelRow = new PixelRow();
                            for (int i = 0; i < 144; i++) {
                                if (!valves[i] && pixels[i].getHeight() > 0) {
                                pixelRow.addPixel(pixels[i]);
                                pixels[i] = getNewPixel(i, se.getColor(), pane);
                            }
                        }

                        pixelRowList.add(pixelRow);
                        lastDropReleased = currentTime;
                        counter++;
                    }

                    if(counter == height){
                        valves = off();
                    }
                }

                for(int i = 0; i < 144; i++) {
                    if(valves[i]){
                        pixels[i].addTime(elapsedTimeS);
                        pixels[i].setHeight(HALFGRAVITY * pixels[i].getTime() * pixels[i].getTime());
                    }
                }

                pixelRowList.forEach(pixelRow -> {
                    pixelRow.getPixelList().forEach(pixel -> {
                        pixel.addTime(elapsedTimeS);
                        pixel.setHeight(HALFGRAVITY * pixel.getTime() * pixel.getTime() - pixelRow.getY());
                        pixel.relocate(pixel.getX(), pixelRow.getY());
                    });
                });

                if (pixelRowList.get(0).getY() > HEIGHT) {
                    pixelRowList.get(0).getPixelList().forEach(x -> pane.getChildren().remove(x));
                    pixelRowList.remove(0);
                }

                if (pixelRowList.isEmpty()) {
                    Event.fireEvent(stage, new Event(ActionEvent.ACTION));
                    this.stop();
                }

                lastIteration = currentTime;
            }
        };
    }

    private void initialize() {

    }

    private void checkPreviousAudio(StreamElement element) {
        StreamElement prevElement = elementList.getList().get(elementList.getList().indexOf(element) - 1);
        if (!prevElement.getAudioTrack().equals(element.getAudioTrack())) {
            if (element.getAudioTrack().equals("")) {
                player.stop();
            } else {
                if (player != null) {
                    player.stop();
                }
                String path = element.getAudioTrack();
                media = new Media(new File(path).toURI().toString());
                player = new MediaPlayer(media);
                player.setVolume(.3);
                player.play();
            }
        }
    }

    public void linkStage(Stage s) {
        stage = s;
    }

    public void start() {
        elementList = ElementList.getInstance();
        stage.addEventHandler(ActionEvent.ANY, newAnimation());
        draw();
    }

    public MediaPlayer getMediaPlayer() {
        return player;
    }

    private Pixel getNewPixel(int i, Color c, Pane pane){
        Pixel p = new Pixel((i*width) + (width - (width*(dropWidth/sectionWidth))) / 2, 0,
                width*(dropWidth/sectionWidth), 0);

        if(c.equals(Color.TRANSPARENT)){
            c = Color.BLACK;
        }
        p.setFill(c);
        pane.getChildren().add(p);
        return p;
    }

    private Pixel[] getNewPixelArray(Color c, Pane pane){
        Pixel[] pixelArray = new Pixel[144];
        for(int i = 0; i < 144; i++) {
            Pixel p = new Pixel((i * width) + (width - (width * (dropWidth / sectionWidth))) / 2, 0,
                    width * (dropWidth / sectionWidth), 0);

            if (c.equals(Color.TRANSPARENT)) {
                c = Color.BLACK;
            }
            p.setFill(c);
            pixelArray[i] = p;
            pane.getChildren().add(p);
        }
        return pixelArray;
    }
    public boolean[] off(){
        boolean[] off = new boolean[144];
        for(int i = 0; i < 144; i++){
            off[i] = false;
        }
        return off;
    }
}
