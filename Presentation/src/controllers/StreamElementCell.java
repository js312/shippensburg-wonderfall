package controllers;

import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ListCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import model.ElementList;
import model.StreamElement;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Controls each element in the timeline
 */
public class StreamElementCell extends ListCell<StreamElement> {
    private static final DataFormat customFormat = new DataFormat("cell");
    private static HBox hbox = new HBox();

    /**
     * Constructor for Stream Element Cell
     */
    public StreamElementCell() {
        ListCell thisCell = this;

        setOnDragDetected(event -> {
            if (getItem() == null) { return; }
            Dragboard dragboard = startDragAndDrop(TransferMode.MOVE);
            ClipboardContent content = new ClipboardContent();
            content.put(customFormat,getItem());
            dragboard.setContent(content);

            event.consume();
        });

        setOnDragOver(event -> {
            if (event.getGestureSource() != thisCell && event.getDragboard().hasContent(customFormat)) {
                event.acceptTransferModes(TransferMode.MOVE);
            }

            event.consume();
        });

        setOnDragEntered(event -> {
            if (event.getGestureSource() != thisCell &&
                    event.getDragboard().hasContent(customFormat)) {
                setOpacity(0.3);
            }
        });

        setOnDragExited(event -> {
            if (event.getGestureSource() != thisCell && event.getDragboard().hasContent(customFormat)) {
                setOpacity(1);
            }
        });

        setOnDragDropped(event -> {
            if (getItem() == null) {
                return;
            }

            Dragboard db = event.getDragboard();
            boolean success = false;

            if (db.hasContent(customFormat)) {
                ElementList items = ElementList.getInstance();
                StreamElement o = (StreamElement) db.getContent(customFormat);
                int draggedIdx = items.getList().indexOf(items.getById(o.getID()));
                int thisIdx = items.getList().indexOf(getItem());

                items.getList().remove(draggedIdx);
                items.getList().add(thisIdx, (StreamElement) db.getContent(customFormat));

                List<StreamElement> itemscopy = new ArrayList<>(getListView().getItems());
                getListView().getItems().setAll(itemscopy);

                success = true;
            }
            event.setDropCompleted(success);

            event.consume();
        });

        setOnDragDone(DragEvent::consume);

    }


    /**
     * Updates an element in the timeline
     * @param item the item to update
     * @param empty
     */
    @Override
    protected void updateItem(StreamElement item, boolean empty) {
        super.updateItem(item, empty);

        if (empty || item == null) {
            setText("");
            setGraphic(new Circle(5, Color.TRANSPARENT));
        } else {
            final Canvas canvas = new Canvas(20,20);
            GraphicsContext gc = canvas.getGraphicsContext2D();

            if (!item.getColor().equals(Color.TRANSPARENT)) {
                gc.setFill(Color.BLACK);
                gc.fillOval(0,0,20,20);
            }

            gc.setFill(item.getColor());
            gc.fillOval(1,1,18,18);

            Rectangle r = new Rectangle();
            r.setWidth(30);
            r.setHeight(30);
            Circle c = new Circle(5, Color.BLACK);
            Text text = new Text(item.convertArrayToString(item.getImageArray()));
            text.setLineSpacing(-.53);
            text.setFont(Font.font("Monospaced", 1));
            if (!item.getAudioTrack().equals("")){
                Path p = Paths.get(item.getAudioTrack());
                String name = p.getFileName().toString();
                Image icon = new Image("resources/music_note.png");
                ImageView view = new ImageView(icon);
                hbox = new HBox(gc.getCanvas(), text, view, new Text(name));
            } else {
                hbox = new HBox(gc.getCanvas(), text);
            }

            hbox.setSpacing(5);
            hbox.setAlignment(Pos.CENTER);
            setGraphic(hbox);
        }
    }
}

