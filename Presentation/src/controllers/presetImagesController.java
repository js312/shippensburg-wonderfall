package controllers;

import commands.AddPresetCommand;
import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import model.StreamElement;

import java.io.FileNotFoundException;

/**
 * The Controller for the Preset Images popup
 */
public class presetImagesController {
    private ListView timeline = new ListView<StreamElement>();

    /**
     * Loads the preset image into the timeline
     * @param actionEvent addOnClick
     */
    public void loadPreset(ActionEvent actionEvent) {
        Button pressed = (Button) actionEvent.getSource();
        AddPresetCommand command = new AddPresetCommand(pressed.getId(), timeline);
        command.execute();
    }

    /**
     * Links the main timeline to the class
     * @param timeline the timeline to link
     */
    public void linkTimeline(ListView timeline) {
        this.timeline = timeline;
    }
}
