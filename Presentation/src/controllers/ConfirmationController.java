package controllers;

import commands.CommandInterface;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * Controller for the confirmation popup
 */
public class ConfirmationController {
    private CommandInterface command;
    @FXML Button cancel;

    /**
     * Close the popup and do nothing
     */
    public void closeWindow() {
        Stage stage = (Stage) cancel.getScene().getWindow();
        stage.close();
    }

    /**
     * execute the correct command then close the popup
     */
    public void confirmAction() {
        command.execute();
        closeWindow();
    }

    /**
     * link the appropriate action to the controller
     * @param command the appropriate command
     */
    public void linkAction(CommandInterface command) {
        this.command = command;
    }
}
