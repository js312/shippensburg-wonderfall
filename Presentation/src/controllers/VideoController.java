package controllers;

import commands.AddImageCommand;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.EdgeDetection;
import model.StreamElement;
import model.StreamImage;
import org.apache.commons.io.IOUtils;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.videoio.VideoCapture;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.*;
import java.util.Timer;
import java.util.TimerTask;


/**
 * Controller class to complete actions for the popup
 * @author Aaron Wertman, Jake Harrington, Eric, Ryan
 */
public class VideoController {
    @FXML private Button start_btn;
    @FXML private Label info_label;
    @FXML private Button confirm_btn;
    @FXML private ImageView currentFrame;
    @FXML private ComboBox<String> comboBox;
    @FXML private Slider brightnessSlider;

    public static final String ONE_FACE = "Only one face is supported at a time";
    public static final String NO_FACE = "No face detected, please retake picture";
    private static final String DEFAULT_FILTER = EdgeDetection.HORIZONTAL_FILTER;

    private Pane rootElement;
    private Timer timer;
    private VideoCapture capture = new VideoCapture();
    private Imgcodecs Highgui;
    private Mat frame = null;
    MatOfRect faceDetections = null;
    private Object selectedItem;
    private ListView timeline;
    TimerTask frameGrabber = null;
    boolean needToCrop = false;
    private File tempFile;

    /**
     * Initializes the Video
     */
    public void initialize() {
        comboBox.getItems().add(EdgeDetection.HORIZONTAL_FILTER);
        comboBox.getItems().add(EdgeDetection.VERTICAL_FILTER);
        comboBox.getItems().add(EdgeDetection.SCHARR_FILTER_HORIZONTAL);
        comboBox.getItems().add(EdgeDetection.SCHARR_FILTER_VERTICAL);
        comboBox.getItems().add(EdgeDetection.SOBEL_FILTER_HORIZONTAL);
        comboBox.getItems().add(EdgeDetection.SOBEL_FILTER_VERTICAL);
        comboBox.setValue(DEFAULT_FILTER);

        selectedItem = DEFAULT_FILTER;

        comboBox.setOnAction((event) -> {
            selectedItem = comboBox.getSelectionModel().getSelectedItem();
        });
    }

    /**
     * Creates a temporary file for opencv to access outside of the jar
     */
    public void createFile() {
        InputStream inputStream = getClass().getResourceAsStream("/resources/lbpcascade_frontalface.xml");

        tempFile = new File("tmp.txt");
        try {
            if (!tempFile.exists()) {
                tempFile.createNewFile();
            }
            FileOutputStream fos = new FileOutputStream(tempFile);
            IOUtils.copy(inputStream, fos);
            fos.close();
            if (tempFile.length() == 0) {
                System.out.println("null");
            }
        } catch (IOException e) {
            System.out.println("io except");
        }
    }

    /**
     * Deletes the file if it exists
     */
    public void deleteFile() {
        File file = new File("tmp.txt");
        if (file.exists()) {
            file.delete();
        }
    }

    /**
     * Start the camera and show the live video with the picture being converted and
     * give option to start, restart, or confirm picture
     */
    @FXML
    protected void startCamera()
    {
        if (tempFile == null) {
            createFile();
        }

        needToCrop = false;
        // check: the main class is accessible?
        if (this.rootElement != null)
        {
            // get the ImageView object for showing the video stream
            final ImageView frameView = currentFrame;
            Stage stage = (Stage) start_btn.getScene().getWindow();

            // check if the capture stream is opened
            if (!this.capture.isOpened())
            {
                // start the video capture
                this.capture.open(0);
                // grab a frame every 33 ms (30 frames/sec)
                frameGrabber = new TimerTask() {
                    @Override
                    public void run()
                    {
                        Image tmp = detectFace();
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run()
                            {
                                 frameView.setImage(tmp);
                            }
                        });
                    }
                };
                this.timer = new Timer();
                //set the timer scheduling, this allow you to perform frameGrabber every 33ms;
                this.timer.schedule(frameGrabber, 0, 33);
                confirm_btn.setVisible(false);
                info_label.setText(ONE_FACE);
                this.start_btn.setText("Take Picture");
                stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent windowEvent) {
                        capture.release();
                        if (timer != null)
                            timer.cancel();

                        deleteFile();
                    }
                });
            }
            else
            {
                needToCrop = true;
                this.start_btn.setText("Retake Picture");
                // stop the timer
                if (this.timer != null)
                {
                    this.timer.cancel();
                    frameGrabber.cancel();
                    this.timer = null;
                }
                // release the camera
                this.capture.release();
                if (faceDetections != null && !faceDetections.empty()) {
                    confirm_btn.setVisible(true);
                } else {
                    info_label.setText(NO_FACE);
                }
                stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                    @Override
                    public void handle(WindowEvent windowEvent) {
                        deleteFile();
                    }
                });
            }
        }
    }

    /**
     * Confirm the picture taken and add it to the timeline
     */
    public void confirmPicture() {
        StreamElement test = new StreamImage(currentFrame.getImage(), "Face");
        new AddImageCommand(timeline, test).execute();
        Stage stage = (Stage) start_btn.getScene().getWindow();
        stage.close();
        deleteFile();
    }

    /**
     * Detect the users face in the image and call the method to draw box around it
     * @return the image of face
     */
    private Image detectFace()
    {
        frame = new Mat();
        BufferedImage image;
        // check if the capture is open
        if (this.capture.isOpened())
        {
            try
            {
                // read the current frame
                this.capture.read(frame);
                drawBoxAroundFace();
                image = getConvertedImage();
                if (needToCrop) {
                    BufferedImage tempCropped = cropImage(image, faceDetections.toArray()[0]);
                    Image newImg = SwingFXUtils.toFXImage(tempCropped, null);
                    currentFrame.setImage(newImg);
                    return newImg;
                }
                return SwingFXUtils.toFXImage(image, null);
            }
            catch (ArrayIndexOutOfBoundsException i) {
                image = getConvertedImage();
                return SwingFXUtils.toFXImage(image, null);
            }
            catch (Exception e)
            {
                // log the error
                System.err.println("ERROR: " + e.getMessage());
            }
        }
        return currentFrame.getImage();
    }

    /**
     * Draw a box around the face to know where the face is
     */
    private void drawBoxAroundFace() {
        /////// Detecting the face in the snap ////
        if (tempFile == null) {
            createFile();
        }

        CascadeClassifier classifier = new CascadeClassifier(tempFile.getAbsolutePath());
        faceDetections = new MatOfRect();
        classifier.detectMultiScale(frame, faceDetections);

        // Drawing boxes
        Rect rect = faceDetections.toArray()[0];
        Imgproc.rectangle(
                frame,                                   //where to draw the box
                new Point(rect.x-10, rect.y - 60),                            //bottom left
                new Point(rect.x + 10 + rect.width, rect.y + 30 + rect.height), //top right
                new Scalar(0, 0, 255)                                 //RGB colour
        );
    }

    /**
     * Use edge detection to convert the image in order for it to be usable in timeline
     * @return the converted image
     */
    private BufferedImage getConvertedImage() {
        BufferedImage image = null;
        try{
            image = new BufferedImage(frame.width(), frame.height(),
                    BufferedImage.TYPE_3BYTE_BGR);
        } catch (IllegalArgumentException e) {
            System.out.println(("Error: " + e));
        }

        WritableRaster raster = image.getRaster();
        DataBufferByte dataBuffer = (DataBufferByte) raster.getDataBuffer();
        byte[] data = dataBuffer.getData();
        frame.get(0, 0, data);

        EdgeDetection detection = new EdgeDetection();
        try {
            image = detection.detectEdges(image, (String)selectedItem, (int)brightnessSlider.getValue());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }

    /**
     * Crop the image to a specified rectangle
     * @param src the source image
     * @param rect the rectangle to crop to
     * @return the new cropped image
     */
    private BufferedImage cropImage(BufferedImage src, Rect rect) {
        BufferedImage dest = src.getSubimage(rect.x-5, rect.y-55, rect.width+10, rect.height+80);
        return dest;
    }

    /**
     * setter for the root element
     * @param root the root element
     */
    public void setRootElement(Pane root)
    {
        this.rootElement = root;
    }

    /**
     * link this timeline to the programs timeline
     * @param timeline the timeline
     */
    public void linkTimeline(ListView timeline) {
        this.timeline = timeline;
    }
}
