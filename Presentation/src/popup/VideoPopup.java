package popup;

import controllers.VideoController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * @author Aaron Wertman, Jake Harrington
 * Class for the video popup window
 */
public class VideoPopup {
    /**
     * Create the popup for the live face cam to take pictures of face for wonderfall
     * @param timeline the projects timeline
     * @throws IOException if the FXML can't be loaded
     */
    public VideoPopup(ListView timeline) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/resources/video.fxml"));
        BorderPane root1 = fxmlLoader.load();
        VideoController controller = fxmlLoader.getController();
        controller.linkTimeline(timeline);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root1));
        stage.setResizable(false);
        stage.setTitle("Wonderfall");
        Image i = new Image("/resources/Shippensburg_State_University_Logo.png");
        stage.getIcons().add(i);
        stage.show();
        controller.setRootElement(root1);
    }
}
