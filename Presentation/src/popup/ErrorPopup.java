package popup;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ErrorPopup {

    /**
     * Opens window when an error occur
     * @param title - the name of window
     * @param msg - the error message you wish to display
     */
    public ErrorPopup(String title, String msg) {
        createWindow(title, msg, 300, 150);
    }

    /**
     * Overloaded to accept width and height of error box
     * @param title - the name of window
     * @param msg - the error message you wish to display
     */
    public ErrorPopup(String title, String msg, int width, int height) {
        createWindow(title, msg, width, height);
    }

    private void createWindow(String title, String msg, int width, int height) {
        Stage popupwindow = new Stage();
        popupwindow.initModality(Modality.APPLICATION_MODAL);
        popupwindow.setTitle(title);
        Image i = new Image("resources/Shippensburg_State_University_Logo.png");
        popupwindow.getIcons().add(i);

        Text t = new Text(msg);
        TextFlow label1 = new TextFlow(t);
        label1.setTextAlignment(TextAlignment.CENTER);

        Button button1 = new Button("Close & Continue");
        button1.setOnAction(e -> popupwindow.close());
        VBox layout = new VBox(10);
        layout.getChildren().addAll(label1, button1);
        layout.setAlignment(Pos.CENTER);
        Scene scene1 = new Scene(layout, width, height);
        popupwindow.setScene(scene1);
        popupwindow.showAndWait();
    }

}
