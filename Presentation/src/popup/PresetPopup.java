package popup;

import controllers.presetImagesController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class PresetPopup {
    /**
     * Opens a window that has preset images ready to choose from.
     * @param timeline listview containing all elements
     * @throws IOException
     */
    public PresetPopup (ListView timeline) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/resources/presetImages.fxml"));
        Parent root1 = (Parent) fxmlLoader.load();
        presetImagesController controller = fxmlLoader.getController();
        controller.linkTimeline(timeline);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root1));
        Image i = new Image("resources/Shippensburg_State_University_Logo.png");
        stage.getIcons().add(i);
        stage.show();
    }
}
