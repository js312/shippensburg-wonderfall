package popup;

import controllers.PreviewController;
import javafx.fxml.FXMLLoader;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class PreviewDisplay {

    /**
     * Shows the Preview Display for the waterfall
     */
    public PreviewDisplay()  {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setController(new PreviewController());
        PreviewController controller = fxmlLoader.getController();
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        controller.linkStage(stage);
        controller.start();
        Image i = new Image("resources/Shippensburg_State_University_Logo.png");
        stage.getIcons().add(i);
        stage.show();
    }













    /**
     * Creates a popup where the user can select from the list of preset images
     * @param s a string containing all the text for images in the timeline
     */
    /*
    public PreviewDisplay(String s) {
        image = s;
        Text msg = new Text(s);
        msg.setStyle("-fx-font: 1px monospace;");
        msg.setLineSpacing(-.5);
        Pane root = new Pane(msg); //Change to file location
        root.setPrefSize(320, 640);
        Scene scene = new Scene(root);

        stage = new Stage();
        stage.setTitle("Preview");
        stage.setScene(scene);
        stage.initModality(Modality.APPLICATION_MODAL);
        Image i = new Image("resources/Shippensburg_State_University_Logo.png");
        stage.getIcons().add(i);

        double h = scene.getHeight();
        double mh = msg.getLayoutBounds().getHeight();
        System.out.println("height: = " + h + " message height: = " + mh);
        KeyValue initKeyValue = new KeyValue(msg.translateYProperty(), -s.lines().count());
        KeyFrame initFrame = new KeyFrame(Duration.ZERO, initKeyValue);

        KeyValue endKeyValue = new KeyValue(msg.translateYProperty(), root.getPrefHeight());
        KeyFrame endFrame = new KeyFrame(Duration.seconds(6), endKeyValue);

        Timeline timeline = new Timeline(initFrame, endFrame);

        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
        stage.showAndWait();
    }
*/


}
