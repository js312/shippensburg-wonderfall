package popup;

import controllers.AudioController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class AudioPopup {

    /**
     * Create the popup for choosing the audio
     * @throws IOException shouldnt
     */
    public AudioPopup(ListView timeline) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/resources/audio.fxml"));
        BorderPane root1 = (BorderPane) fxmlLoader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root1));
        stage.setResizable(false);
        stage.setTitle("Wonderfall");
        Image i = new Image("/resources/Shippensburg_State_University_Logo.png");
        stage.getIcons().add(i);
        AudioController controller = fxmlLoader.getController();
        controller.linkTimeline(timeline);
        stage.show();
    }
}
