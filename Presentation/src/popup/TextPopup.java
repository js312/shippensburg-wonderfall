package popup;

import controllers.TextPopupController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class TextPopup {

    /**
     * Shows the prompts for the Text popup
     * @param timeline The timeline the elements are in
     * @throws IOException if the FXML can't be loaded
     */
    public TextPopup (ListView timeline) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/resources/TextPopup.fxml"));
        Parent root1 = fxmlLoader.load();
        TextPopupController controller = fxmlLoader.getController();
        controller.linkTimeline(timeline);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.setScene(new Scene(root1));
        Image i = new Image("resources/Shippensburg_State_University_Logo.png");
        stage.getIcons().add(i);
        stage.show();
    }
}
