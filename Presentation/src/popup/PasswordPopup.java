package popup;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class PasswordPopup {

    /**
     * Shows the password prompt for power mode
     * @throws IOException If the FXML can't be found / loaded
     */
    public PasswordPopup() throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/resources/password.fxml"));
        Parent root1 = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root1));
        stage.setResizable(false);
        stage.setTitle("Wonderfall");
        Image i = new Image("/resources/Shippensburg_State_University_Logo.png");
        stage.getIcons().add(i);
        stage.show();
    }
}
