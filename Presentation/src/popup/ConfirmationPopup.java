package popup;

import commands.CommandInterface;
import controllers.AudioController;
import controllers.ConfirmationController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Creates the popup for the confirmation window
 */
public class ConfirmationPopup {

    /**
     * Shows the confirmation popup
     * @throws IOException If the FXML can't be found / loaded
     */
    public ConfirmationPopup(CommandInterface command) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/resources/confirmation.fxml"));
        Parent root1 = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setScene(new Scene(root1));
        stage.setResizable(false);
        stage.setTitle("Wonderfall");
        Image i = new Image("/resources/Shippensburg_State_University_Logo.png");
        stage.getIcons().add(i);
        ConfirmationController controller = fxmlLoader.getController();
        controller.linkAction(command);
        stage.show();
    }
}
