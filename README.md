# Wonderfall

**LucidChart UML**:
https://lucid.app/lucidchart/invitations/accept/acc28546-fe4f-445a-88ae-f179102f2c57

**Google Jamboard**
https://jamboard.google.com/d/1bBbdzI9jLd32EAsFyxmogQHG-FQjvMa3t7FyjuYoQAM/viewer?ts=606c7a40&f=2

**Mural**:
https://app.mural.co/t/planninggame0194/m/planninggame0194/1614114028792/75cd11f7d3a3e606e7d49729cf86d8054aec9cd8

**YouTrack**:
https://youtrack.engr.ship.edu:8443/dashboard

**Setting up JavaFX**:
https://openjfx.io/openjfx-docs/

**Java JDK Version**:
https://www.oracle.com/java/technologies/javase-jdk11-downloads.html
*  Java SE Development Kit 11.0.10, Installer or Compressed Archive

**JavaFX Version**: 
https://gluonhq.com/products/javafx/
*  Long Term Support -> JavaFX Windows SDK -> Public version 11.0.2 -> Download


**Setting up the Executable**:
Adding required library:
* File->Project Structure->Libraries->"+"->From Maven
* Search for commons-io, once the list pops up choose commons-io:commons-io:2.8.0, Ok

Once complete, navigate to:
* File->Project Structure->Artifacts

Once in that menu:
* "+"->Jar->From Modules with Dependencies
* Module: Wonderfall
* Main Class: JarMain
* "Ok"

Click on that .jar in the artifacts menu:
* "+" with a drop down -> File
* Add opencv_java452.dll
* Add all .dll files from javafx-sdk-11.0.2\bin
* Once added, apply, Ok, and close that window

At the top:
* Build->Build Artifacts->Build
* The executable should be complete now


*  Build Project
    *  Under Edit Configurations for JarMain
    *  Modify Options - Add VM Options
        *  --module-path "Your path to JavaFX lib folder" --add-modules javafx.controls,javafx.fxml, 
        *  --add-modules javafx.controls,javafx.media, 
        *  -Dprism.lcdtext=false

**Jsch Version**:
*  http://www.jcraft.com/jsch/
*  Version: 0.1.55
*  Under project structure
    *  Modules - Dependencies - Click the plus - Add Jars or Directories - add the jar

**OpenCV Version**: 
https://opencv.org/releases/
*  4.5.2
*  Intellij -> project structure -> modules -> Wonderfall -> dependencies
    *  add JAR -> opencv/build/java/opencv-452.jar -> OK
    *  double click the new jar -> "+" -> add opencv/build/java/x64/opencv_java452.dll -> OK
