package SSH;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import java.io.ByteArrayOutputStream;

/**
 * The SSH tunnel class, this class runs on start up to create a connection
 * between the Raspberry PI and the software side. It also is able to open channels
 * to send commands through the tunnel to the PI
 *
 * todo:
 * Add SSH credentials for the PI
 * Change the destination for where the files should go in SendFile()
 */
public class SSHTunnel {
    /**
     * Instance Variables:
     * @session - the current ssh session
     * @hostName - the remote host to connect to
     * @user - user name for remote host
     * @password - password for remote host
     */
    static Session session = null;
    static String hostName = ""; //ip
    static String user = ""; //user name (if it has one)
    static String password = ""; //password (if it has one)

    /**
     * Creates an SSH connection to the PI
     * @throws Exception if it can't find the server
     */
    public static void createConnection() throws Exception {
        int portNumber = 22;

        session = new JSch().getSession(user, hostName, portNumber);
        session.setPassword(password);
        session.setConfig("StrictHostKeyChecking", "no"); //change to yes when implementing connection to PI if needed
        session.connect();
        System.out.println("User " + user + " is Connected to " + hostName + " on port " + portNumber);
    }

    /**
     * Opens a channel and allows commands to be sent through the tunnel.
     * Closes the channel after it executes the command
     * @param command the command to execute
     * @throws Exception
     */
    public static void commandExecute(String command) throws Exception {
        ChannelExec executionStream = null;
        try{

            executionStream = (ChannelExec) session.openChannel("exec");
            executionStream.setCommand(command);
            ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
            executionStream.setOutputStream(responseStream);
            executionStream.connect();

            while(executionStream.isConnected()){
                Thread.sleep(100);
            }

            String responseString = new String(responseStream.toByteArray());
            System.out.println(responseString);
        } finally {
            if(executionStream != null){
                System.out.println("Channel Disconnected...");
                executionStream.disconnect();
            }
        }

    }

    /**
     * @param filePath the path of the file to be sent via ssh
     * @throws Exception
     * establishes sftp channel to send files and sends given file
     */
    public static void sendFile(String filePath) throws Exception{
        ChannelSftp sftpChannel = null;
        try{
            sftpChannel = (ChannelSftp) session.openChannel("sftp");
            sftpChannel.connect();

            sftpChannel.put(filePath, ""); //change destination when implementing this to PI ssh server
            System.out.println("File Sent...");
        }finally{
            if(sftpChannel != null){
                System.out.println("sftp channel disconnect");
                sftpChannel.disconnect();
            }
        }
    }

    /**
     * Closes the SSH connection to the PI
     */
    public static void closeConnection(){
        if(session != null){
            System.out.println("Session Disconnected from " + hostName + "...");
            session.disconnect();
        }
    }
}
