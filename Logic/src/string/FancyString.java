package string;


import java.awt.Font;
import java.io.Serializable;

public class FancyString implements Serializable {
    String element;
    Font font;

    public FancyString(String str, Font f){
        element = str;
        font = f;
    }

    public Font getFont() {
        return font;
    }

    public String getString(){
        return element;
    }

    @Override
    public String toString(){ return element; }

}
