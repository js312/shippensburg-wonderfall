package SSHCommands;

import SSH.SSHTunnel;

public class RunDefaultCommand implements CommandInterface{

    @Override
    public void execute() {
        SSHTunnel ssh = new SSHTunnel();
        String command = ""; //the command
        String filename = ""; //the default file name to run

        try{
            ssh.commandExecute(command);
        }catch(Exception ex){
            System.out.println(ex);
        }
    }
}
