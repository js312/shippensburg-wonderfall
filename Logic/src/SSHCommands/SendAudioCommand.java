package SSHCommands;

import SSH.SSHTunnel;
import javafx.scene.control.ListView;
import model.StreamElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Sends all audio for the timeline through the SSH tunnel
 * @author Jun Pan and Logan Cole
 */
public class SendAudioCommand implements CommandInterface {

    private final ListView timeline;
    private List<String> usedAudios = new ArrayList<String>();
    private SSHTunnel ssh = new SSHTunnel();
    public SendAudioCommand(ListView timeline) {
        this.timeline = timeline;
    }

    /**
     * Executes the command
     */
    @Override
    public void execute() {
        int i = 0;
        while(i < timeline.getItems().size()){
            int j = 0;
            StreamElement element = (StreamElement) timeline.getItems().get(i);
            String audioTrack = element.getAudioTrack();
            boolean foundAudio = false;
            while(j < usedAudios.size() && !foundAudio) {
                if(usedAudios.get(j).equals(audioTrack)) {
                    foundAudio = true;
                }
                j++;
            }
            if (!foundAudio && !audioTrack.equals("")) {
                usedAudios.add(audioTrack);
                try{
                    ssh.sendFile(audioTrack);
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
            i++;
        }
    }
}