package SSHCommands;

import javafx.scene.control.ListView;
import javafx.scene.paint.Color;
import model.StreamElement;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * The Command to write the timeline to the file
 * @author Logan Cole, Jun Pan
 */
public class WriteFileCommand implements CommandInterface {
    private final int delay = 5;
    private final File file = new File("Story.txt");
    private ListView timeline;

    /**
     * Constructor for the WriteFileCommand
     * @param timeline the timeline to write to the file
     */
    public WriteFileCommand(ListView timeline) {
        this.timeline = timeline;
    }

    /**
     * Writes to the file to be sent through the SSH tunnel
     */
    @Override
    public void execute() {
        String[] picture;
        String audio = " ";
        Color color;
        String redString, greenString, blueString;
        try {
            FileWriter myWriter = new FileWriter(file);

            for (int i = 0; i < timeline.getItems().size(); i++) {
                StreamElement element = (StreamElement) timeline.getItems().get(i);
                color = element.getColor();
                if (!element.getAudioTrack().equals("") && !element.getAudioTrack().equals(audio)){
                    String s = element.getAudioTrack();
                    s = s.substring(s.lastIndexOf("\\") + 1);
                    myWriter.write("start_audio " + s + " 65 1\n");
                }
                picture = element.convertToHexArray(element.getImageArray());
                redString = Integer.toHexString((int)Math.round(color.getRed() * 255));
                greenString = Integer.toHexString((int)Math.round(color.getGreen() * 255));
                blueString = Integer.toHexString((int)Math.round(color.getBlue() * 255));
                // ensures that each color has 2 digits in hex
                if (redString.length() == 1) {
                    redString = '0' + redString;
                }
                if (greenString.length() == 1) {
                    greenString = '0' + greenString;
                }
                if (blueString.length() == 1) {
                    blueString = '0' + blueString;
                }
                String RGBValues = redString + greenString + blueString + "\n";
                for(int j = picture.length-1; j >= 0; j--){
                    myWriter.write("pattern 0x" + picture[j] + " " + delay + " 0x" + RGBValues);
                }

                myWriter.write("delay 10\n");
                if (i < timeline.getItems().size()-1){
                    StreamElement element2 = (StreamElement) timeline.getItems().get(i+1);
                    if (element.getAudioTrack().equals("")) {
                        // don't write stop_audio if there is no audio file for current cell
                    }
                    else if (!audio.equals(element2.getAudioTrack())) {
                        myWriter.write("stop_audio\n");
                    }
                } else {
                    if (!element.getAudioTrack().equals("")) {
                        myWriter.write("stop_audio\n");
                    }
                }
            }
            myWriter.close();
            new SendFileCommand(file).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}