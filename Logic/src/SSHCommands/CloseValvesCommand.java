package SSHCommands;

import SSH.SSHTunnel;

public class CloseValvesCommand implements CommandInterface{

    @Override
    public void execute() {
        String command = "close all valves";
        SSHTunnel ssh = new SSHTunnel();
        try{
            ssh.commandExecute(command);
        }catch(Exception ex){
            System.out.println(ex);
        }
    }
}
