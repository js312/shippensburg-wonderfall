package SSHCommands;

import SSH.SSHTunnel;

public class OpenValvesCommand implements CommandInterface{

    @Override
    public void execute() {
        String command = "open all valves";
        SSHTunnel ssh = new SSHTunnel();
        try{
            ssh.commandExecute(command);
        }catch(Exception ex){
            System.out.println(ex);
        }
    }
}
