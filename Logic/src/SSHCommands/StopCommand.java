package SSHCommands;

import SSH.SSHTunnel;

public class StopCommand implements CommandInterface{

    @Override
    public void execute() {
        String command = "stop";
        SSHTunnel ssh = new SSHTunnel();
        try{
            ssh.commandExecute(command);
        }catch(Exception ex){
            System.out.println(command);
        }
    }
}
