package SSHCommands;

import SSH.SSHTunnel;

import java.io.File;

/**
 * Sends the file through the SSH Tunnel
 */
public class SendFileCommand implements CommandInterface{
    private File story;
    public SendFileCommand(File story){
        this.story = story;
    }

    /**
     * Executes the command
     */
    @Override
    public void execute() {
        SSHTunnel ssh = new SSHTunnel();

        try{
            ssh.sendFile(story.getPath());
        }catch(Exception ex){
            System.out.println(ex);
        }
    }
}
