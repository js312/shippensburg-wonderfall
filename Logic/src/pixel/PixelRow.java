package pixel;

import javafx.scene.paint.Color;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class PixelRow {
    private List<Pixel> pixelList;
    private double yCoordinate;
    private double velocity;
    private double currentTime;
    private final double GRAVITY = 3200; //32ft/s^2     100px = 1ft so 32 * 100 = GRAVITY
    final private double width = 5;           //pixels wide.  width of the window/number of valves
    final private double dropWidth = .2;      //the diameter of the valve
    final private double sectionWidth = .625; //Number of inches wide / number of valves

    /**
     *
     * @param pixels row of pixels
     */
    public PixelRow(boolean[] pixels, double time , javafx.scene.paint.Color c) {
        pixelList = new ArrayList<Pixel>();
        for(int i = 0; i < pixels.length; i++) {
            if (pixels[i]) {
                //The width is our current step +
                // (the width of each section - the width of a drop in pixels then divided by 2)
                // to get it in the center of each section.
                Pixel p = new Pixel((i*width) + (width - (width*(dropWidth/sectionWidth))) / 2, 0,
                        width*(dropWidth/sectionWidth), Math.max(GRAVITY*time*time/2, 2));
                if(c.equals(Color.TRANSPARENT)){
                    c = Color.BLACK;
                }
                p.setFill(c);
                pixelList.add(p);
            }
        }
        yCoordinate = 0.0;
        velocity = 0.0;
        currentTime = 0.0;
    }

    public PixelRow(){
        pixelList = new ArrayList<Pixel>();
    }

    public void addPixel(Pixel p){
        pixelList.add(p);
    }


    /**
     * Get the pixels that are being tracked.
     * @return The list of pixels
     */
    public List<Pixel> getPixelList(){
        return pixelList;
    }

    /**
     * Get the y Coordinate for the whole row.
     * @return the Y Coordinate
     */
    public double getY(){
        return yCoordinate;
    }

    /**
     * calculate the new Y Position
     * @param time time to calculate
     */
    public void calculateNewY(double time){
        velocity += GRAVITY * time;
        yCoordinate += velocity * time;
    }
}
