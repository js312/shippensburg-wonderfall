package pixel;

import javafx.scene.shape.Rectangle;

public class Pixel extends Rectangle {
    private double time;
    public Pixel(double x, double y, double width, double height){
        super(x, y, width, height);
        time = 0;
    }

    public double getTime(){
        return time;
    }

    public void addTime(double t){
        time += t;
    }
    //public Pixel(int x){
       // xCoordinate = x;
   // }

    //public int getX(){
        //return xCoordinate;
    //}
}
