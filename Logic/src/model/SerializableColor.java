package model;

import java.io.Serializable;
import javafx.scene.paint.Color;

/**
 * Converts the color to a serializable color
 * @Author Aaron, Jake, Eric, Logan, Jun
 */
public class SerializableColor implements Serializable
{
    private double red;
    private double green;
    private double blue;
    private double alpha;

    /**
     * Constructor for the serializable color
     * @param color the color chosen
     */
    public SerializableColor(Color color)
    {
        this.red = color.getRed();
        this.green = color.getGreen();
        this.blue = color.getBlue();
        this.alpha = color.getOpacity();
    }

    /**
     * @return the color chosen
     */
    public Color getFXColor()
    {
        return new Color(red, green, blue, alpha);
    }
}