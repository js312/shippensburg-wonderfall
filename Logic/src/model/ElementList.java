package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Logic Layer timeline
 */
public class ElementList {
    private static ElementList uniqueInstance = null;
    private static ObservableList<StreamElement> elements = FXCollections.observableArrayList();

    private ElementList() {}

    /**
     * Singleton of the timeline
     * @return the instance
     */
    public static ElementList getInstance() {
        if (uniqueInstance == null) {
            uniqueInstance = new ElementList();
        }
        return uniqueInstance;
    }

    /**
     * Gets an element by the id given
     * @param id the id of the element
     * @return the element
     */
    public StreamElement getById(double id) {
        for (StreamElement e: elements) {
            if (e.getID() == id) return e;
        }
        return null;
    }

    /**
     * Adds an element to the logic layer list
     * @param i the element to add
     */
    public void addElement(StreamElement i) {
        elements.add(i);
    }

    /**
     * gets the logic layer list of elements
     * @return the elements
     */
    public ObservableList<StreamElement> getList() {
        return elements;
    }

    /**
     * Sets the current presentation layer elements to the logic layer elements
     * @param list the list of elements to set
     */
    public void setList(ObservableList<StreamElement> list) {
        elements.setAll(list);
    }
}
