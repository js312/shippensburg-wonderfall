package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import popup.ErrorPopup;
import java.io.*;

/**
 * A class that can save a story of stream elements to a binary file and load a story from a binary file
 */
public class SaveLoadSystem {

    /**
     * Saves a story of stream elements into a binary text file
     * @param list the list of stream elements
     * @param file the file to be saved into
     */
    public static void saveStory(ObservableList<StreamElement> list, File file) {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeInt(list.size());
            for(int i = 0; i < list.size(); i++) {
                out.writeObject(list.get(i));
            }
            out.close();
            fileOut.close();
        }
        catch (FileNotFoundException e) {
            new ErrorPopup("Save Failed", "Save file not found");
        } catch (IOException e) {
            new ErrorPopup("Save Failed", "Save write failed, please try again");
        }
        new ErrorPopup("File saved!", "Saved in: " + file.getPath(), 500, 150);
    }

    /**
     * Loads a story of stream elements from a file
     * @param file the file that contains the story
     * @return the list of stream elements
     */
    public static ObservableList<StreamElement> loadStory(File file) {
        ObservableList<StreamElement> list = FXCollections.observableArrayList();
        try {
            FileInputStream fileIn = new FileInputStream(file);
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            int size = objectIn.readInt();
            for(int i = 0; i < size; i++) {
                list.add((StreamElement) objectIn.readObject());
            }
        } catch (FileNotFoundException e) {
            new ErrorPopup("Load Failed", "File not found");
        } catch (IOException e) {
            new ErrorPopup("Load Failed", "Load read failed");
        } catch (ClassNotFoundException e) {
            new ErrorPopup("Load Failed", "Please try again");
        }
        return list;
    }
}
