package model;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.nio.Buffer;

import bw_conversion.OtsuThresholder;

/**
 * Scales an image for the preview
 */
public class StreamImage extends StreamElement {

    /**
     * Constructor for Stream Image
     * @param image the image
     * @param imageName the name of the image
     */
    public StreamImage(Image image, String imageName) {
        super(imageName);
        actualImage = SwingFXUtils.fromFXImage(image, null);
        actualImage = scale(actualImage, NUM_OF_PIXELS);

        actualImage = convertToGrayScale(actualImage);
        actualImage = OtsuThresholder.convertToBWImage(actualImage);

        convertImage();
        textImage = convertArrayToString(imageArray);
    }

    public static BufferedImage convertToGrayScale(BufferedImage image) {
        BufferedImage result = new BufferedImage(
                image.getWidth(),
                image.getHeight(),
                BufferedImage.TYPE_BYTE_GRAY);
        Graphics g = result.getGraphics();
        g.drawImage(image, 0, 0, null);
        g.dispose();
        return result;
    }

    /**
     * Scales the image to the specified width, keeps proportions
     * @param imageToScale an image to be converted
     * @param dWidth the desired width of the image
     * @return the scaled image
     */
    public static BufferedImage scale(BufferedImage imageToScale, int dWidth) {
        BufferedImage scaledImage = null;
        double scaleFactor = (double)dWidth / imageToScale.getWidth();
        int dHeight = (int)Math.round(imageToScale.getHeight() * scaleFactor);
        if (imageToScale != null) {
            scaledImage = new BufferedImage(dWidth, dHeight, imageToScale.getType());
            Graphics2D graphics2D = scaledImage.createGraphics();
            graphics2D.drawImage(imageToScale, 0, 0, dWidth, dHeight, null);
            graphics2D.dispose();
        }
        return scaledImage;
    }
}
