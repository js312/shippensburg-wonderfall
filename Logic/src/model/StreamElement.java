
package model;


import javafx.scene.paint.Color;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * The individual elements in the timeline
 */
public abstract class StreamElement implements Serializable {

    public static final int NUM_OF_PIXELS = 144;
    static final int HEIGHT = 200;
    static double nextId = 0;
    boolean[][] imageArray;
    transient BufferedImage actualImage;
    private String imageName;
    String textImage = null;
    double id;
    SerializableColor color = new SerializableColor(Color.TRANSPARENT);
    static ArrayList<String> audioPaths = new ArrayList<>();
    String audioTrack = "";

    /**
     * Constructor for Stream Element
     * @param imageName The name of the image for the specific element
     */
    public StreamElement(String imageName) {
        this.imageName = imageName;
        id = nextId++;
    }

    /**
     * Converts the element to a string array of hex
     * @param streamElements the element to convert
     * @return the String[] of lines in hex
     */
    public String[] convertToHexArray(boolean[][] streamElements){
        String[] pictureHex = new String[streamElements.length];
        for(int row = 0; row < streamElements.length; row++){
            int col = 0;
            String tempString = "";
            while(col < streamElements[0].length){
                int tempNum = 0;
                // checks 4 numbers at a time in binary starting on the left
                // 8 4 2 1
                // if the number of the position we are at is a 1, we will add the according number
                // to tempNum and convert the int to a hexString after 4 numbers
                int start = 8;
                for(int x = 0; x < 4; x++) {
                    if(streamElements[row][col+x]) {
                        tempNum = start + tempNum;
                        start = start/2;
                    }else {
                        start = start/2;
                    }
                }
                tempString = tempString + Integer.toHexString(tempNum);
                col+=4;
            }
            pictureHex[row] = tempString;
        }
        return pictureHex;
    }

    /**
     * Gets the id of the element
     * @return the id
     */
    public double getID() {
        return id;
    }

    /**
     * Sets the id of the element following the current
     * @param next the id for the element
     */
    public void setNextId(double next) {
        nextId = next;
    }

    /**
     * gets all the audio paths for the timeline
     * @return the array list of audio paths
     */
    public static ArrayList<String> getAudioPaths() {
        return audioPaths;
    }

    /**
     * Gets the buffered image for the element
     * @return the image
     */
    public BufferedImage getBufferedImage() {
        return actualImage;
    }

    /**
     * Converts actualImage to an int array
     * First checks the image dimensions to ensure they're within specifications, if not scales it down to proportional size
     * and the max width is 144 pixels then converts to a boolean array
     */
    public void convertImage() {
        // convert image to bitmap
        int width = actualImage.getWidth();
        int height = actualImage.getHeight();
        boolean[][] result = new boolean[height][width];

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                int colors = actualImage.getRGB(col, row);
                int r = (colors >> 16) & 0xff;
                int g = (colors >> 8) & 0xff;
                int b = colors & 0xff;
                if (r == 0 && g == 0 && b == 0) {
                    result[row][col] = true;
                } else {
                    result[row][col] = false;
                }
            }
        }
        imageArray = result;
    }

    /**
     * Add a path for an audio track to the list
     * @param audioPath the path to add to the list
     */
    public void addAudioToList(String audioPath){
        for(String path : audioPaths){
            if(audioPath.equals(path)){
                return;
            }
        }
        audioPaths.add(audioPath);
    }

    /**
     * getter for the Audio track
     * @return the audio track for the element
     */
    public String getAudioTrack(){
        return audioTrack;
    }

    /**
     * Set the audio track for the element
     * @param audio the path for the audio track
     */
    public void setAudioTrack(String audio) {
        audioTrack = audio;
    }

    /**
     * Returns a bitmap image array
     * @return bitmap image array
     */
    public boolean[][] getImageArray() {
        return imageArray;
    }

    /**
     * Converts 2D boolean array to string.
     * @param imageArray 2D boolean array
     * @return String
     */
    public String convertArrayToString(boolean[][] imageArray) {
        String string = "";
        if (imageArray != null) {
            for (int i = 0; i < imageArray.length; i++) {
                for (int j = 0; j < imageArray[i].length; j++) {
                    if (imageArray[i][j]) {
                        string += '■';
                    } else
                        string += " ";
                }
                string += "\n";
            }
        }
        return string;
    }

    /**
     * Inverts the element's pixels to the opposite
     * of what they are set
     */
    public void invertStreamElement(){
        for(int i = 0; i < imageArray.length; i++){
            for(int j = 0; j < imageArray[i].length; j++){
                imageArray[i][j] = !imageArray[i][j];
            }
        }
        textImage = convertArrayToString(imageArray);
    }

    /**
     * Gets the string version of the element
     * @return the text version of the image
     */
    public String getTextImage() {
        return textImage;
    }

    /**
     * Set the color of the lights for this element
     * @param color the color of the lights
     */
    public void setColor(Color color) {
        this.color = new SerializableColor(color);
    }

    /**
     * @return the color of lights associated with this element
     */
    public Color getColor() {
        if (color.getFXColor() == null) {
            return Color.TRANSPARENT;
        }
        return color.getFXColor();
    }

    @Override
    public String toString() {
        return imageName;
    }
}
