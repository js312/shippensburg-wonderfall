package model;

import string.FancyString;
import exceptions.StringTooLongException;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * The class for formatting text in the timeline
 */
public class StreamText extends StreamElement {
    int numCharacters;
    FancyString stringToConvert;

    /**
     * Constructor for StreamText
     * @param stringToConvert string to convert
     */
    public StreamText(FancyString stringToConvert) throws StringTooLongException{
        super(stringToConvert.getString());
        this.stringToConvert = stringToConvert;
        numCharacters = stringToConvert.getString().length();
        actualImage = new BufferedImage(NUM_OF_PIXELS, HEIGHT, BufferedImage.TYPE_INT_RGB);
        convertToImage();
    }

    /**
     * Converts string to buffered image and scales image based on string size.
     */
    public void convertToImage() throws StringTooLongException {
        Graphics2D trans = (Graphics2D) actualImage.getGraphics();
        //This is done a second time so we can set the height otherwise it just uses the height variable in StreamElement.
        //Which makes it too big.
        actualImage = new BufferedImage(NUM_OF_PIXELS, getHeight(trans), BufferedImage.TYPE_INT_RGB);
        Graphics2D trans2 = (Graphics2D) actualImage.getGraphics();

        if(getWidth(trans2) > NUM_OF_PIXELS){
            throw new StringTooLongException();
        }

        drawCenteredString(trans2, stringToConvert, new Rectangle(NUM_OF_PIXELS, getHeight(trans2)));
        convertImage();
        textImage = convertArrayToString(imageArray);
    }

    /**
     * Gets the width of the text
     * @param g the text
     * @return the width of the string
     */
    public int getWidth(Graphics g)
    {
        FontMetrics metrics = g.getFontMetrics(stringToConvert.getFont());
        return metrics.stringWidth(stringToConvert.getString());
    }

    /**
     * Gets the height of the text
     * @param g the text
     * @return the height of the text
     */
    public int getHeight(Graphics g)
    {
        FontMetrics metrics = g.getFontMetrics(stringToConvert.getFont());
        return metrics.getHeight();
    }

    /**
     * Centers the text for the preview
     * @param g the text
     * @param fs the fancy string
     * @param rect the rectangle to put the text in
     */
    public void drawCenteredString(Graphics g, FancyString fs, Rectangle rect) {
        FontMetrics metrics = g.getFontMetrics(fs.getFont());
        g.fillRect(0, 0, NUM_OF_PIXELS, getHeight(g));
        int x = rect.x + (rect.width - metrics.stringWidth(fs.getString())) / 2;
        int y = rect.y + ((rect.height - metrics.getHeight()) / 2) + metrics.getAscent();
        g.setFont(fs.getFont());
        g.setColor(Color.BLACK);
        g.drawString(fs.getString(), x, y);
    }
}