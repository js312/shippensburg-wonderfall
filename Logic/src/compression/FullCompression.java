package compression;

import model.StreamElement;

public class FullCompression {
    private static final int GRAVITY = 3200;
    private static final double HALFGRAVITY = 0.5 * GRAVITY;
    private static final double HORIZANTALGAP = 3.4; //Pixels
    private static final double MIDDLEPOINT = 375;
    private StreamElement streamElement;

    public FullCompression(StreamElement se){
        streamElement = se;
    }

    public double calculateFullDropRate() {
        //int middlePoint = (int)( 5 * (0.5 * streamElement.getImageArray().length));
        double middleTime = Math.sqrt(MIDDLEPOINT / HALFGRAVITY);
        System.out.println(middleTime);
        double dropRate = ((GRAVITY * middleTime) - Math.sqrt(GRAVITY * (GRAVITY * Math.pow(middleTime,2) - 2 *
                HORIZANTALGAP))) / GRAVITY;
        System.out.println(dropRate);
        return dropRate;
    }


}
