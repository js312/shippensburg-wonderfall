package compression;

import model.StreamElement;
import pixel.PixelRow;

public class VariableCompression {
    private double largestGap;
    private static final double GRAVITY = 3200; //intellij sugma
    private static final double HALFGRAVITY = 0.5 * GRAVITY;
    private int rowCount = 0;
    private double dropRate = 0.022;
    private StreamElement streamElement;
    private double bottom = 750;
    private double dropTime;

    /**
     *
     * @param streamElement the streamElement to be compressed.
     */
    public VariableCompression(StreamElement streamElement) {
        this.streamElement = streamElement;
        this.dropTime = bottomAtPoint();
        calculateGap();
    }

    /**

    private void calcLargestGap(float timeLast, float fastDrop) {
        largestGap = HALFGRAVITY*(timeLast*timeLast - (timeLast-fastDrop)*(timeLast-fastDrop));
        rowCount +=2;
    }

    private double dropRate(double prevTime) {
        return (GRAVITY*prevTime + Math.sqrt(GRAVITY*((GRAVITY*(prevTime*prevTime))-2*largestGap)) / GRAVITY);
    }
    */

    /**
     * Calculates the time it takes for a "droplet" to fall dis
     * @return time in seconds
     */
    private double bottomAtPoint(){
        return Math.sqrt(bottom / HALFGRAVITY);
    }

    /**
     * Calculate the time difference to get the required gap
     * @param prevTime - the time of the previous row
     * @return time in seconds
     */
    private double calculateTime(double prevTime) {
        return (GRAVITY * prevTime - Math.sqrt(GRAVITY * (GRAVITY * Math.pow(prevTime, 2) - 2 * largestGap))) / GRAVITY;
    }

     /** @return the gap between the bottom two rows of our 2d array
     * uses 1/2at^2 (second bottom row) - 1/2at^2 (bottom row) to determine this
     */
    private void calculateGap(){
        largestGap = HALFGRAVITY*(dropTime*dropTime) - (HALFGRAVITY*((dropTime/*-dropRate*/)*(dropTime/*-dropRate*/)));
    }

    /**
     * For each row in the image, calculate the gap between rows and add that time to the array.
     * time passed in to calculateTime should be sum of start time and all current gaps within the array.
     * @return an array that is filled with times to print each PixelRow
     */   
    public double[] createTimeArray(){
        int length = streamElement.getImageArray().length;
        double[] timeArray = new double[length];
        double currentTime = dropTime;
        //System.out.println(dropTime);

        for(int i = 0; i < length; i++) {
            timeArray[i] = calculateTime(currentTime);
            currentTime += timeArray[i];
        }
        return timeArray;
    }


}


