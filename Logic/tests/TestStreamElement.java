import commands.InvertStreamElementCommand;
import exceptions.StringTooLongException;
import javafx.scene.image.Image;
import model.StreamElement;
import model.StreamImage;
import model.StreamText;
import org.junit.Test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class TestStreamElement {

    @Test
    public void testScale() throws IOException {
        final int NUM_OF_PIXELS = 320;

        Image image = new Image(
                new FileInputStream("Presentation/src/resources/Shippensburg_State_University_Logo.png"));
        StreamImage imageClass = new StreamImage(image, "Ship logo");
        BufferedImage buffImage = StreamImage.scale(imageClass.getBufferedImage(), NUM_OF_PIXELS);
        double scaleFactor = (double)NUM_OF_PIXELS/ image.getWidth();
        int dHeight = (int)Math.round(image.getHeight() * scaleFactor);
        assertEquals(dHeight, buffImage.getHeight());
        assertEquals(NUM_OF_PIXELS, buffImage.getWidth());
    }

    @Test
    public void testPrintImageName() throws FileNotFoundException {
        File file = new File("Presentation/src/resources/Shippensburg_State_University_Logo.png");
        Image image = new Image(
                new FileInputStream(file));
        StreamElement streamElement = new StreamImage(image, file.getName());
        assertEquals("Shippensburg_State_University_Logo.png", streamElement.toString());
    }

    @Test
    public void testChickyNuggies() throws FileNotFoundException {
        //StreamElement se = new StreamText("Chicky Nuggies");
        //System.out.print(se.getTextImage());
        Image image = new Image(
                new FileInputStream("Presentation/src/Preset_Images/ship.png"));
        StreamImage testImage = new StreamImage(image, "Ship logo");
        testImage.convertImage();

        boolean[][] normalArray = testImage.getImageArray();
        String[] hex = testImage.convertToHexArray(normalArray);
        for (int i = 0; i < hex.length; i++){
            System.out.println(hex[i]);
        }
    }

    @Test
    public void testInvert() throws FileNotFoundException {
        Image image = new Image(
                new FileInputStream("Presentation/src/Preset_Images/ship.png"));
        StreamImage testImage = new StreamImage(image, "Ship logo");
        testImage.convertImage();

        boolean[][] normalArray = testImage.getImageArray();
        new InvertStreamElementCommand(testImage).execute();
        boolean[][] invertArray = testImage.getImageArray();

        for(int i = 0; i < normalArray.length; i++){
            for(int j = 0; j < normalArray[i].length; j++){
                assertEquals(normalArray[i][j], invertArray[i][j]);
            }
        }

    }
}
