import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;
import model.SaveLoadSystem;
import model.StreamElement;
import model.StreamImage;
import org.junit.Before;
import org.junit.Test;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static org.junit.Assert.assertEquals;

/**
 * Test class to ensure that the save and load story methods are working as intended
 */
public class TestSaveLoadSystem {

    /**
     * First checks that a previous test file does not exist, if so it deletes it and creates a new one
     * with the specified images and runs saveStory(). Whether saveStory worked will be tested in the loadStory()
     * test method
     * @throws FileNotFoundException file does not exist
     */
    @Before // Ran before the both test methods
    public void setupSaveStory() throws FileNotFoundException {
        try {
            File testFile = new File("TestSave.ser");
            if (!testFile.exists()) {
                throw new FileNotFoundException();
            }
            testFile.delete();
        }
        catch (FileNotFoundException e) {
            System.out.println("No file, continuing test");
        }

        javafx.scene.image.Image image = new Image(new FileInputStream("Presentation/src/resources/ship.png"));
        StreamImage imageClass1 = new StreamImage(image, "ship.png");
        image = new Image(new FileInputStream("Presentation/src/resources/tiger.png"));
        StreamImage imageClass2 = new StreamImage(image, "tiger.png");

        ObservableList<StreamElement> list = FXCollections.observableArrayList();
        list.add(imageClass1);
        list.add(imageClass2);

        File file = new File("TestSave.ser");
        SaveLoadSystem.saveStory(list, file);
    }

    /**
     * Loads the story into the list, then checks if everything in the stream elements were
     * retrieved successfully.
     */
    @Test
    public void testLoadStory() {
        File testFile = new File("TestSave.ser");
        ObservableList<StreamElement> list = SaveLoadSystem.loadStory(testFile);
        assertEquals(2, list.size());
        assertEquals("ship.png", list.get(0).toString());
        assertEquals("tiger.png", list.get(1).toString());
    }
}
